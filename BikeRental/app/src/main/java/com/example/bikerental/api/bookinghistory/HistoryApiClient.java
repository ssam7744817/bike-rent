package com.example.bikerental.api.bookinghistory;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.booking.BookingApiInterface;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class HistoryApiClient {
    private static String BookingHistoryApiUrl="";
    private static HistoryApiInterface apiInterface;

    /**
     * create or return (if exists) connect to server
     *
     * @return: AccountApiInterface
     */
    public static HistoryApiInterface getApiClient() {
        if (apiInterface == null) {

            getServerUrl();

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BookingHistoryApiUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(HistoryApiInterface.class);
        }
        return apiInterface;
    }

    /**
     * get correct url for account api
     */
    private static void getServerUrl() {
        BookingHistoryApiUrl = Instance.BikeUrl;
        Log.d("BookingApiUrl", "getServerUrl: " + BookingHistoryApiUrl);
    }
}
