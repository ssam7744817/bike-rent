package com.example.bikerental.api.geocoding;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.model.geocoding.BikeLocation;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class GeocodingApiHome {
    private static final String TAG = "GeocodingApiClient";
    // api account
    private GeocodingApiInterface mApiAccount = GeocodingApiClient.getApiClient();
    public void getLocation(String address,final ApiResponseResult callback){
        Call<BikeLocation> call = mApiAccount.getLocation(address,Instance.API_KEY);
        call.enqueue(new Callback<BikeLocation>() {
            @Override
            public void onResponse(Response<BikeLocation> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "GeocodingApiClient: success " );

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "GeocodingApiClient: me: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "GeocodingApiClient: me: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
