package com.example.bikerental.contract;

import com.example.bikerental.api.model.store.Store;

public class StoreActivityContract {
    public interface presenter{
        void getAllStore();
    }
    public interface view{
        void setStoreItemToRecycleView(Store store);
    }
}
