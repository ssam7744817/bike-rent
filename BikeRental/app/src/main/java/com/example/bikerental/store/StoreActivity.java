package com.example.bikerental.store;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bikerental.R;

public class StoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
    }
}
