package com.example.bikerental.api.bookinghistory;

import android.util.Log;

import com.example.bikerental.api.model.history.BookingHistory;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class HistoryApiHome {
    private static final String TAG = "HistoryApiHome";
    // api account
    private HistoryApiInterface mApiAccount = HistoryApiClient.getApiClient();
    public void getBookingHistory(String userId,final ApiResponseResult callback){
        Call<BookingHistory> call = mApiAccount.getBookingHistory(userId);
        call.enqueue(new Callback<BookingHistory>() {
            @Override
            public void onResponse(Response<BookingHistory> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "HistoryApiClient: success " );

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "HistoryApiClient: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "HistoryApiClient: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
