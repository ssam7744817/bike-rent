package com.example.bikerental.api.store;

import com.example.bikerental.api.model.road.Road;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface StoreApiInterface {
    @GET("branches")
    Call<Store> getAllStore(

    );
    @GET("branches/{store_id}")
    Call<ListBranch> getStoreById(@Path("store_id")String storeId

    );

}
