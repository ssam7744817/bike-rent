package com.example.bikerental.test;

public interface SharedPreferencesRepository {
    boolean isLoggedIn();
}
