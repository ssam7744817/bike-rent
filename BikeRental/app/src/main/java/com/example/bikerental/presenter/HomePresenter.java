package com.example.bikerental.presenter;

import android.util.Log;

import com.example.bikerental.api.bikes.BikesApiHome;
import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.contract.HomeContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;


public class HomePresenter {
    final String TAG = "BikesApiClient";
    private HomeContract.view view;
    private Bikes bike;

    public HomePresenter(HomeContract.view view) {
        this.view = view;
    }
//
//    @Override
//    public void getBikeList() {
//        view.showDialog();//Mo dialog trong luc load cai list
//        new BikesApiHome().getBikeList(new ApiResponseResult() {
//            @Override
//            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
//                switch (var1) {
//                    case SUCCESS:
//                        setBike((Bikes) var2);
//                        view.setBikeToRecycleView(getBike());
//                        view.hideDialog();
//                        Log.d(TAG, "onResponse success: ");
//                        break;
//                    case FAIL:
//                        Log.d(TAG, "onResponse fail: ");
//                        break;
//                    case NETWORK_PROBLEM:
//                        Log.d(TAG, "onResponse network prolem: ");
//
//                        break;
//                }
//            }
//        });
//    }

    public Bikes getBike() {
        return bike;
    }

    public void setBike(Bikes bike) {
        this.bike = bike;
    }
}
