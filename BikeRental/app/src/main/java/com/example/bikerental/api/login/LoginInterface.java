package com.example.bikerental.api.login;

import com.example.bikerental.api.model.geocoding.BikeLocation;
import com.example.bikerental.api.model.user.User;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Query;

public interface LoginInterface {
    @GET("service/google/")
    Call<User> login(
            @Header("token") String token
    );
}
