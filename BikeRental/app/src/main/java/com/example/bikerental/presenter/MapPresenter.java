package com.example.bikerental.presenter;

import android.icu.text.Collator;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.bikerental.api.geocoding.GeocodingApiHome;
import com.example.bikerental.api.model.geocoding.AddressComponent;
import com.example.bikerental.api.model.geocoding.BikeLocation;
import com.example.bikerental.api.model.road.Road;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;
import com.example.bikerental.api.road.RoadAPIHome;
import com.example.bikerental.api.store.StoreApiHome;
import com.example.bikerental.contract.MapContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class MapPresenter implements MapContract.Presenter {
    private final static int MAXIMUM_TIME = 3000;
    private final static int COUNTDOWN_TIME = 1000;
    private final String TAG = "SAM";
    private Place currentPlace;
    private List<Marker> storeNearBy;
    private List<ListBranch> storeInDistrict = new ArrayList<>();
    private Road road;

    public List<ListBranch> getStoreInDistrict() {
        return storeInDistrict;
    }

    public void setStoreInDistrict(List<ListBranch> storeInDistrict) {
        this.storeInDistrict = storeInDistrict;
    }

    public Road getRoad() {
        return road;
    }

    public void setRoad(Road road) {
        this.road = road;
    }

    private FindCurrentPlaceResponse findCurrentPlaceResponse;
    private Store store;
    PlacesClient placesClient;
    private BikeLocation addressDetail;// Tra ve thong tin chi tiet ma google map tim tu geocoding api
    private MapContract.View view;

    public MapPresenter(PlacesClient place, MapContract.View view) {
        this.view = view;
        this.placesClient = place;
    }

    public FindCurrentPlaceResponse getFindCurrentPlaceResponse() {
        return findCurrentPlaceResponse;
    }

    public void setFindCurrentPlaceResponse(FindCurrentPlaceResponse findCurrentPlaceResponse) {
        this.findCurrentPlaceResponse = findCurrentPlaceResponse;
    }

    /**
     * Ta se lay vi tri dau tien, roi bo no vao geocoding de lay place
     * tu do ta se giai quyet dc moi van de.
     *
     * @param placesClient
     */
    @Override
    public void getCurrentPlace(PlacesClient placesClient) {
        // Use fields to define the data types to return.
        List<Place.Field> placeFields = Collections.singletonList(Place.Field.NAME);

// Use the builder to create a FindCurrentPlaceRequest.
        FindCurrentPlaceRequest request =
                FindCurrentPlaceRequest.newInstance(placeFields);
        Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
        placeResponse.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FindCurrentPlaceResponse response = task.getResult();
                setFindCurrentPlaceResponse(response);
                for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
                    Log.d(TAG, "getCurrentPlace: name= " + placeLikelihood.getPlace().getName());
//                    getLocationDetail(placeLikelihood.getPlace().getName());
//                    if (getAddressDetail().getStatus().equals("OK")) {
//                        break;
//                    }
                    Log.d(TAG, "\n" + "getCurrentPlace: address name= " + placeLikelihood.getPlace().getName() + "\n" + "address= " + placeLikelihood.getPlace().getAddress());
                }
//                getLocationDetail();
                /**
                 * Chuyen doi ten lay tu placeLikeHood thanh location detail
                 */
            } else {
                Exception exception = task.getException();
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    Log.e(TAG, "Place not found: " + apiException.getStatusCode() + " message: " + apiException.getMessage());
                }
            }
        });
    }


    /**
     * Lay ten tu Placeclient de tim location detail
     *
     * @param
     */
    @Override
    public void getLocationDetail(String addressName) {
        view.showDialog();
//        String addressName = getFindCurrentPlaceResponse().getPlaceLikelihoods().get(0).getPlace().getName();
        Log.d(TAG, "getLocationDetail: dia chi tim chi tiet= " + addressName);
        new GeocodingApiHome().getLocation(addressName, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1) {
                    case SUCCESS:
                        BikeLocation user=(BikeLocation)var2;
                        if(user.getStatus().equals("OK")){
                            Log.d(TAG, "onResponse: success getLocationDetail");
                            setAddressDetail(user);
                            BikeLocation location = getAddressDetail();
                            Log.d(TAG, "location detail distritc: " + location.getResults().get(0).getAddressComponents().get(0).getTypes());
                            getAllStore();
                        }else{
                            view.showGPSProlem();
                        }
                        break;
                    case FAIL:
                        Log.d(TAG, "onResponse: fail");

                        break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "onResponse: network_prolem");

                        break;
                }
            }
        });

    }

    /**
     * Lay danh sach cac cua hang
     */
    @Override
    public void getAllStore() {
        new StoreApiHome().getAllStore(new ApiResponseResult() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1) {
                    case SUCCESS:
                        setStore((Store) var2);
                        Log.d(TAG, "getAllStore: success");
//                Log.d(TAG, "onResponse:store size "+getStore().getListBranches().size()+"district= "+getAddressDetail().getResults().get(0).getAddressComponents().get(2).getShortName());
                        List<AddressComponent> list = getAddressDetail().getResults().get(0).getAddressComponents();
                        String district = "";
                        for (AddressComponent addressComponent : list) {
                            for (String string : addressComponent.getTypes()) {
                                if (string.equals("administrative_area_level_2")) {
                                    getStoreByDistrict(addressComponent.getShortName());
                                }
                            }
                        }
                        break;
                    case FAIL:
                        Log.d(TAG, "getAllStore: fail");

                        break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "getAllStore: Network");

                        break;
                }
            }
        });
    }

    /**
     * Tao 1 list cac marker hien thi cua hang trong quan
     */
//    @Override
//    public void readyMarker(GoogleMap mMap) {
//
//    }

    /**
     * Ta se so sanh quan cua vi tri hien tai voi quan cua cac cua hang
     * nen giong nhau se add vao list moi
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void getStoreByDistrict(String district) {
        int count = 0;

        // Get the Collator for US English and set its strength to PRIMARY
        Collator usCollator = Collator.getInstance(Locale.US);
        usCollator.setStrength(Collator.PRIMARY);

        List<ListBranch> listBranch = getStore().getListBranches();
        for (ListBranch item : listBranch) {
            Log.d(TAG, "district name tu Danh: " + item.getDistrict().trim() + " district tu server= " + district.trim());
            if (usCollator.compare(item.getDistrict(), district) == 0) {
                Log.d(TAG, "2 quan bang nhau: ");
                getStoreInDistrict().add(item);
            }
            count++;
            if (count == listBranch.size()) {
                Log.d(TAG, "Bang roi: ");
                view.hideDialog();
                view.readyMarker();
            } else {
                Log.d(TAG, "thua roi:count=  " + count);
            }
        }
    }

    @Override
    public void getRoadInfo(String origin,String destination) {
        new RoadAPIHome().getRoadInfo(origin, destination, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1) {
                    case SUCCESS:
                        Log.d(TAG, "getRoadInfo: success");
                        Road road = (Road) var2;
                        setRoad(road);
                        view.showRoadInfo();
                        break;
                    case FAIL:
                        Log.d(TAG, "getRoadInfo: fail");

                        break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "getRoadInfo: net");

                        break;
                }
            }
        });
    }

    @Override
    public void cooldown(String orgin) {
        /**
         * this method help you make a coutdown for resend code when SMS timeout
         * the SMS time out will be set in sendVerificationCode()
         */

        new CountDownTimer(MAXIMUM_TIME, COUNTDOWN_TIME) {
            @Override
            public void onTick(long millisUntilFinished) {
//                view.showDialog();
//                getCurrentPlace(placesClient);
                getLocationDetail(orgin);
            }

            @Override
            public void onFinish() {
                view.finishScreen();
            }
        }.start();

    }

    public BikeLocation getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(BikeLocation addressDetail) {
        this.addressDetail = addressDetail;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public void setCurrentPlace(Place currentPlace) {
        this.currentPlace = currentPlace;
    }

    public List<Marker> getStoreNearBy() {
        return storeNearBy;
    }

    public void setStoreNearBy(List<Marker> storeNearBy) {
        this.storeNearBy = storeNearBy;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
