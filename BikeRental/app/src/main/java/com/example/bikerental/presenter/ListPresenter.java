package com.example.bikerental.presenter;

import android.util.Log;
import android.widget.Toast;

import com.example.bikerental.Instance;
import com.example.bikerental.api.bikes.BikesApiHome;
import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bikev2.BikeV2;
import com.example.bikerental.contract.BikeListContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

public class ListPresenter implements BikeListContract.presenter {
    final String TAG = "SAM";
    private BikeListContract.view view;
    private Bikes bike;

    public ListPresenter(BikeListContract.view view) {
        this.view = view;
    }

    public Bikes getBike() {
        return bike;
    }

    public void setBike(Bikes bike) {
        this.bike = bike;
    }

    @Override
    public void getBikeList() {
        Log.d(TAG, "getBikeList: "+"\n"+
                "pick_up_date: "+Instance.start_time+"\n"+
                "return_date: "+Instance.end_time+"\n"+
                "transmission_type: "+Instance.loai_xe+"\n"
               );
        view.showDialog();
        new BikesApiHome().getBikeList(Instance.start_time,Instance.end_time,String.valueOf(Instance.loai_xe),new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                view.hideDialog();
                switch (var1) {
                    case SUCCESS:
                        setBike((Bikes) var2);
                        Log.d(TAG, "getBikeList onResponse success: ");
                        if(getBike().getListBike().size()>0){
                            view.setBikeToRecycleView(getBike());
//                        view.hideDialog();
//                            Log.d(TAG, "getBikeList onResponse success: ");
                        }else{
                            view.showToast();
                        }
                        break;
                    case FAIL:
                        Log.d(TAG, "getBikeList onResponse fail: ");
                        break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "getBikeList onResponse network prolem: ");

                        break;
                }
            }
        });
    }
}
