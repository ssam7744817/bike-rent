package com.example.bikerental.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.bikerental.contract.BikeDetailActivityContract;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

public class BikeDetailPresenter implements BikeDetailActivityContract.presenter {
    private FirebaseAuth firebaseAuth;
    private final String TAG="ID_tokken";
    private BikeDetailActivityContract.DetailView view;
    public BikeDetailPresenter(FirebaseAuth auth, BikeDetailActivityContract.DetailView view){
        this.firebaseAuth=auth;
        this.view=view;
    }

    /**
     * Neu nhu ko co token thi app se biet la tai khoan da out
     */
    @Override
    public void checkUserSignIn() {
        if(firebaseAuth.getCurrentUser()!=null){
            firebaseAuth.getCurrentUser().getIdToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()){
                        if(task.getResult().getToken()!=null||!task.getResult().getToken().isEmpty()){
                            view.goToUserInfoInput();
                        }
                    }
                }
            });
        }else{
            view.goToSignInOption();
        }
    }
}
