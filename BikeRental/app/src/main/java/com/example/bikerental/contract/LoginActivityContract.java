package com.example.bikerental.contract;

public class LoginActivityContract {

public interface FirebaseSignIn{
      void checkGoogleAccountValid();
      void checkGoogleAccountIsExist();
      void checkInfoValid(String name,String phone);
    void getUserInfo(String token);

//      void loginByApp(String token);
    }
    public interface LoginView{
    void showGoogleProgressBar();
    void hideGoogleProgressBar();
    void showToast(String s);
    void hideProgressBar();
    void goToInputUserInfo();
    void finishActivity();
    void loadLoginScreen();
    void showProgressBar();
    void showGoogleSignInButton();
    void hideGoogleSignInButton();
    void showContinueButton();
    void hideContinueButton();
    void saveUserId(String userId
    );
    }
    }


