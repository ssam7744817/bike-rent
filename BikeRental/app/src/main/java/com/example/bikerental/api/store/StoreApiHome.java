package com.example.bikerental.api.store;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.model.road.Road;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;
import com.example.bikerental.api.road.RoadAPIClient;
import com.example.bikerental.api.road.RoadAPIInterface;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class StoreApiHome {
    private static final String TAG = "StoreAPIClient";
    // api account
    private StoreApiInterface mApiAccount = StoreApiClient.getApiClient();
    public void getAllStore(final ApiResponseResult callback){
        Call<Store> call = mApiAccount.getAllStore();
        call.enqueue(new Callback<Store>() {
            @Override
            public void onResponse(Response<Store> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "onResponse: getAllStore " + response.body().getListBranches().get(0).getAddress());

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "onResponse: getAllStore: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "onResponse getAllStore: NETWORK_PROBLEM: " + t.toString());
            }
        });
    } public void getStoreById(String storeId,final ApiResponseResult callback){
        Call<ListBranch> call = mApiAccount.getStoreById(storeId);
        call.enqueue(new Callback<ListBranch>() {
            @Override
            public void onResponse(Response<ListBranch> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "onResponse: getStoreById " + response.body().getAddress());

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "onResponse: getStoreById: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "onResponse getStoreById: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
