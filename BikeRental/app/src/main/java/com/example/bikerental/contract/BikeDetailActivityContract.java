package com.example.bikerental.contract;


public class BikeDetailActivityContract {

    public interface DetailView {
        void goToUserInfoInput();
        void goToSignInOption();
void getBikeDetail();
    }
    public interface presenter{
        void checkUserSignIn();
    }

}
