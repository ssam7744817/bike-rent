package com.example.bikerental.api.booking;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.bikes.BikesApiInterface;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class BookingApiClient {
    private static String BookingApiUrl="";
    private static BookingApiInterface apiInterface;

    /**
     * create or return (if exists) connect to server
     *
     * @return: AccountApiInterface
     */
    public static BookingApiInterface getApiClient() {
        if (apiInterface == null) {

            getServerUrl();

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BookingApiUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(BookingApiInterface.class);
        }
        return apiInterface;
    }

    /**
     * get correct url for account api
     */
    private static void getServerUrl() {
        BookingApiUrl = Instance.BikeUrl;
        Log.d("BookingApiUrl", "getServerUrl: " + BookingApiUrl);
    }
}
