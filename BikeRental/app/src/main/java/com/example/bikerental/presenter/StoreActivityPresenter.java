package com.example.bikerental.presenter;

import android.util.Log;

import com.example.bikerental.api.model.bike.Branch;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;
import com.example.bikerental.api.store.StoreApiHome;
import com.example.bikerental.contract.StoreActivityContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import java.util.ArrayList;
import java.util.List;

public class StoreActivityPresenter implements StoreActivityContract.presenter {
    private StoreActivityContract.view view;
    private final String TAG="SAM";
    private Store store;
    public StoreActivityPresenter(StoreActivityContract.view view){
        this.view=view;
    }
    @Override
    public void getAllStore() {
        new StoreApiHome().getAllStore(new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1){
                    case SUCCESS:
                        Log.d(TAG, "getAllStore recycleview: success");
                       setStore((Store)var2);
                        view.setStoreItemToRecycleView(getStore());
                        break;
                    case FAIL:
                        Log.d(TAG, "getAllStore recycleview: fail");
break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "getAllStore recycleview: network");
break;
                }
            }
        });
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
