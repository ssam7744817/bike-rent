package com.example.bikerental.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.bikerental.Instance;
import com.example.bikerental.MainActivity;
import com.example.bikerental.R;
import com.example.bikerental.contract.LoginActivityContract;
import com.example.bikerental.getuserinfo.GetUserInfoActivity;
import com.example.bikerental.presenter.LoginPresenter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity implements LoginActivityContract.LoginView, View.OnClickListener {
    LoginActivityContract loginActivityContract;
    private static final int RC_SIGN_IN = 123;
    private final String TAG = "LOGIN";
    Intent intent = new Intent();
    Button googlSingIn;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private GoogleSignInAccount account;
    private EditText getName, getPhone;
    private TextView gmail, login;
    LoginPresenter loginPresenter;
    Button signIn, continueGetInfo;
    private String profile = "";// dung de check coi login dc goi tu Activity nao
    AlertDialog dialog;
    AlertDialog dialogGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Lay du lieu tu intent
        Intent intent = getIntent();
        profile = intent.getStringExtra(Instance.profile);
        Log.d(TAG, "onCreate: profile= " + profile);
        initLayoutAndVariable();
    }

    /**
     * Config cho viec sign in bang google account
     *
     * @return
     */
    public GoogleSignInOptions initGoogleSignIn() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

    }

    /**
     * biding layout
     */
    private void initLayoutAndVariable() {
        getName = findViewById(R.id.getName);
        getPhone = findViewById(R.id.getPhone);
        login = findViewById(R.id.login);
        signIn = findViewById(R.id.sing_in);
        gmail = findViewById(R.id.gmail_already_sign);
        googlSingIn = findViewById(R.id.google_signin);
        continueGetInfo = findViewById(R.id.continue_get_info);
        googlSingIn.setOnClickListener(this);
        continueGetInfo.setOnClickListener(this);
        login.setOnClickListener(this);
        dialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.hop_le).build();
        dialogGoogle = new SpotsDialog.Builder().setContext(this).setTheme(R.style.tai_google_option).build();

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();//neu nhu dang nhap roi thi ko hien trang login nua
        loginPresenter = new LoginPresenter(this, account, user, auth, profile);
        mGoogleSignInClient = GoogleSignIn.getClient(this, initGoogleSignIn());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        hideGoogleProgressBar();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            showProgressBar();
            /**
             * Dang nhap bang tai khoan google da thanh cong
             * tiep theo ta phai xac thuc no thong qua firebase
             */
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                if (data != null) {
                    // lay account
                    loginPresenter.setAcct(task.getResult(ApiException.class));
                    // gui accoutn nay den firebase de xac thuc
                    loginPresenter.checkGoogleAccountIsExist();
                    //Sau khi kiem tra
                } else {
                    Log.d(TAG, "data null ");
                }
                // ...
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                hideProgressBar();
                showToast("Hay chon 1 tai khoan de dang nhap");

            }
        }
    }

    @Override
    public void showGoogleProgressBar() {
        dialogGoogle.show();
    }

    @Override
    public void hideGoogleProgressBar() {
        if (dialogGoogle.isShowing()) {
            dialogGoogle.dismiss();
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideProgressBar() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void goToInputUserInfo() {
        startActivity(new Intent(this, GetUserInfoActivity.class));
    }

    @Override
    public void finishActivity() {
        /**
         * Neu dang nhap thanh cong ma toi tu trang Profile thi ta se gui ve
         * 1 user object de hien thi thong tin
         */
        Log.d(TAG, "finishActivity: da vao");
        Intent intent = new Intent();
        intent.putExtra(Instance.USER, loginPresenter.getUserApp());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void loadLoginScreen() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void showProgressBar() {
        dialog.show();
    }

    @Override
    public void showGoogleSignInButton() {

    }

    @Override
    public void hideGoogleSignInButton() {

    }

    @Override
    public void showContinueButton() {

    }

    @Override
    public void hideContinueButton() {

    }

    @Override
    public void saveUserId(String userId) {
        // File chia sẻ sử dụng trong nội bộ ứng dụng, hoặc các ứng dụng được chia sẻ cùng User.
        SharedPreferences sharedPreferences = this.getSharedPreferences(Instance.SharedToken, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Instance.firebaseUserId, userId);
        editor.apply();
        Log.d(TAG, "saveToken: Da luu thanh cong token= " + userId);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (profile != null) {
            if (profile.equals("profile")) {
                setResult(Activity.RESULT_CANCELED);
            }
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.google_signin:
                mGoogleSignInClient.signOut();//Lam the nay de hien dc bang chon account
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                //truoc khi no mo 1 activity nho de hien cac tai khoan
                showGoogleProgressBar();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.continue_get_info:
                startActivity(new Intent(this, GetUserInfoActivity.class));
                break;
            case R.id.login:

                break;
        }
    }
}
