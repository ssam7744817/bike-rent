package com.example.bikerental;

import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.bikev2.ListBike1;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.user.User;

public class Instance {
    public static final String API_KEY = "AIzaSyAJjxsERZFRBahCsBgp2QWE9Ft4p_tEE9c";
    public static final String contentType = "application/json";
    public static String token="";
    public static String firebaseUserId ="Firebase token";
    public static String SharedToken="Shared token";
    public static User user=new User();
    public static String USER="user object";
    public static String BIKE="bike";
    public static String UNITS="metric";
    public static String baseRoadUrl="https://maps.googleapis.com/maps/api/distancematrix/";
    public static String storeUrl="http://rent-bike-api-2019.herokuapp.com/";
    public static String firebase_cloud_message_chanel_id="Channel human readable title";
    public static String BikeUrl="https://rent-bike-api-2019.herokuapp.com/";
    public static String geocodingUrl="https://maps.googleapis.com/maps/api/geocode/";
    public static String profile="ProfileActivity";
    public static ListBike bike=new ListBike();
    public static ListBranch branch=new ListBranch();
    public static String tai_cua_hang="tai cua hang";
    public static String address="";
    public static String distance="distance";
    public static String branchID="branch ID";
    public static String userAddress="User address";
    public static String userLat="User Lat";
    public static String userLng="User Lng";
    public static String branchAddress="Branch address";
    public static int loai_xe=0;
    public static String start_time="thoi gian bat dau";
    public static String end_time="thoi gian ket thuc";

}
