
package com.example.bikerental.api.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Booking {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("bike")
    @Expose
    private String bike;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("bookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("receiveAddress")
    @Expose
    private String receiveAddress;
    @SerializedName("receiveLongtitude")
    @Expose
    private String receiveLongtitude;
    @SerializedName("receiveLatitude")
    @Expose
    private String receiveLatitude;
    @SerializedName("pickUpDate")
    @Expose
    private String pickUpDate;
    @SerializedName("returnDate")
    @Expose
    private String returnDate;
    @SerializedName("returnAddress")
    @Expose
    private String returnAddress;
    @SerializedName("returnLongtitude")
    @Expose
    private String returnLongtitude;
    @SerializedName("returnLatitude")
    @Expose
    private String returnLatitude;
    @SerializedName("bookingStatus")
    @Expose
    private Integer bookingStatus;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("fullname")
    @Expose
    private String fullname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBike() {
        return bike;
    }

    public void setBike(String bike) {
        this.bike = bike;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    public String getReceiveLongtitude() {
        return receiveLongtitude;
    }

    public void setReceiveLongtitude(String receiveLongtitude) {
        this.receiveLongtitude = receiveLongtitude;
    }

    public String getReceiveLatitude() {
        return receiveLatitude;
    }

    public void setReceiveLatitude(String receiveLatitude) {
        this.receiveLatitude = receiveLatitude;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnAddress() {
        return returnAddress;
    }

    public void setReturnAddress(String returnAddress) {
        this.returnAddress = returnAddress;
    }

    public String getReturnLongtitude() {
        return returnLongtitude;
    }

    public void setReturnLongtitude(String returnLongtitude) {
        this.returnLongtitude = returnLongtitude;
    }

    public String getReturnLatitude() {
        return returnLatitude;
    }

    public void setReturnLatitude(String returnLatitude) {
        this.returnLatitude = returnLatitude;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}
