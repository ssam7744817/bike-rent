package com.example.bikerental.presenter;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.bikerental.Instance;
import com.example.bikerental.api.login.LoginApiHome;
import com.example.bikerental.api.model.user.User;
import com.example.bikerental.contract.LoginActivityContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginPresenter implements LoginActivityContract.FirebaseSignIn {
    private String activityName = "";
    private static String TAG = "HAHA";
    private String token="";
    private LoginActivityContract.LoginView loginView;
    private FirebaseUser user;
    private User userApp;
    private GoogleSignInAccount acct;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    public LoginPresenter(LoginActivityContract.LoginView loginView, GoogleSignInAccount account, FirebaseUser user, FirebaseAuth auth, String name) {
        this.user = user;
        this.acct = account;
        this.mAuth = auth;
        this.loginView = loginView;
        this.activityName = name;
    }

    public String getToken() {
        return token;
    }

    public User getUserApp() {
        return userApp;
    }

    public void setUserApp(User userApp) {
        this.userApp = userApp;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }


    public GoogleSignInAccount getAcct() {
        return acct;
    }

    public void setAcct(GoogleSignInAccount acct) {
        this.acct = acct;
    }

    public FirebaseAuth getmAuth() {
        return mAuth;
    }

    public void setmAuth(FirebaseAuth mAuth) {
        this.mAuth = mAuth;
    }

    @Override
    public void checkGoogleAccountValid() {
        Log.d("SAM", "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener((Activity) loginView, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    user = mAuth.getCurrentUser();
                    if (user != null) {
                        //tat dialog di khi ket thuc xac thuc,
                        user.getIdToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                            @Override
                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "onComplete: token= " + task.getResult().getToken());
                                    /**
                                     * Sau khi dang nhap thanh cong o firebase
                                     * thi ta tiep tuc dang nhap bang api cua app
                                     * de lay user info, id va token cua no moi dang ki dc
                                     */
                                    getUserInfo(task.getResult().getToken());
                                }
                            }
                        });

                    } else {
                        loginView.showToast("Ko tim thay tai khoan");
                    }
                } else {
                    Log.d(TAG, "isFail");
                }
            }
        });
    }

    @Override
    public void checkGoogleAccountIsExist() {
        if (acct == null) {
            loginView.showToast("Hay chon tai khoan");
        } else {
            /**
             * Tiep the la dung LoginPresenter de xac thuc tai khoan
             */
            checkGoogleAccountValid();
        }
    }

    @Override
    public void checkInfoValid(String name, String phone) {

    }

    /**
     * Lay thong tin nguoi dung tu token cua firebase tra ve
     *
     * @param token
     */
    @Override
    public void getUserInfo(String token) {
        new LoginApiHome().login(token, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1) {
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "onResponse: getUserInfo Net");
                        break;
                    case FAIL:
                        Log.d(TAG, "onResponse: getUserInfo FAIL");
                        break;
                    case SUCCESS:
                        Log.d(TAG, "onResponse: getUserInfo SUCCESS");
                        /**
                         * Sau khi ta da lay dc info, ta luu lai token trong shared preference
                         */
                        User user=(User)var2;
                        loginView.saveUserId(user.getId());
                        setUserApp(user);// Luu user lai de su dung cho trang profile
                        Log.d(TAG, "onResponse: user id which have been save= "+user.getId());
                        loginView.hideProgressBar();
                        if (activityName != null) {
                            if (activityName.equals("profile")) {
                                loginView.finishActivity();
                            }
                        } else {
                            loginView.goToInputUserInfo();

                        }
                        Log.d(TAG, "onResponse: user id= " + Instance.user.getId());
                        break;
                }
            }
        });
    }

//    @Override
//    public void loginByApp(String token) {
//        new LoginApiHome().login(token, new ApiResponseResult() {
//            @Override
//            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
//                switch (var1){
//                    case SUCCESS:
//                        //Lay user tra ve sau khi da dang nhap thanh cong
//                        User user=(User)var2;
//                        Instance.user=user;
//                        break;
//                    case FAIL:break;
//                    case NETWORK_PROBLEM:break;
//                }
//            }
//        });
//    }


}
