package com.example.bikerental.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.R;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.bikev2.ListBike1;
import com.example.bikerental.presenter.BikeListPresenter;
import com.squareup.picasso.Picasso;


public class BilkeAdapter extends RecyclerView.Adapter<BilkeAdapter.BikeHolder> {
    private final BikeListPresenter presenter;
    private final OnItemClickListener listener;//bắt sự kiện click
    private final String TAG = "SAM";

    public BilkeAdapter(BikeListPresenter presenter, OnItemClickListener listener) {
        this.listener = listener;
        this.presenter = presenter;
    }

    public interface OnItemClickListener {
        //        void onItemClick(Request item);
        void onItemClick(ListBike item);
    }

    @NonNull
    @Override
    public BikeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BikeHolder view = new BikeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bike_item, parent, false));
        return view;
    }

    @Override
    public void onBindViewHolder(@NonNull BikeHolder holder, int position) {
        presenter.onBindBikeRowViewAtPosition(position, holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Adpter đã nhận ");
                listener.onItemClick(presenter.getBike(position)
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return presenter.getBikeItemCount();
    }

    public class BikeHolder extends RecyclerView.ViewHolder implements BikeListPresenter.BikeItemView {
        TextView bikeName;
        TextView bikePrice,typeTransmission;
        ImageView imageView;

        public BikeHolder(@NonNull View itemView) {
            super(itemView);
            bikePrice = itemView.findViewById(R.id.price);
            bikeName = itemView.findViewById(R.id.bike_name);
//            typeTransmission=itemView.findViewById(R.id.tr);
            imageView = itemView.findViewById(R.id.image);
        }

        @Override
        public void setName(String name) {
            bikeName.setText(name);
        }

        @Override
        public void setPrice(String price) {
            bikePrice.setText(price);
        }

        @Override
        public void setImage(String image) {
            Picasso.get().load(image).into(imageView);
        }

        @Override
        public void setRent(String rent) {
//            typeTransmission.setText(rent);
        }


    }
}
