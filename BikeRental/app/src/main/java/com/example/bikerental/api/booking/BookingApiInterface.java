package com.example.bikerental.api.booking;

import com.example.bikerental.api.booking.newbookingapi.Bookingv2;
import com.example.bikerental.api.model.booking.Booking;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;

public interface BookingApiInterface {
    @POST("bookings/")
    Call<Booking> bookingABike(@Body Bookingv2 bookOrder);
}
