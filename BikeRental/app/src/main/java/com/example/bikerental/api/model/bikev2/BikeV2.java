
package com.example.bikerental.api.model.bikev2;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BikeV2 {

    @SerializedName("listBike1")
    @Expose
    private List<ListBike1> listBike1 = null;
    @SerializedName("total")
    @Expose
    private Integer total;

    public List<ListBike1> getListBike1() {
        return listBike1;
    }

    public void setListBike1(List<ListBike1> listBike1) {
        this.listBike1 = listBike1;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
