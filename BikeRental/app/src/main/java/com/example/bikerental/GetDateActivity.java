package com.example.bikerental;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetDateActivity extends AppCompatActivity {
TextView dayReceive, dayReturn;
    CalendarView calendarView;
    Button dateOk;
    final String EXTRA_DATA="extra";
    private final String TAG="SAM";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_date);
        initLayout();
    }

    private void initLayout() {
        calendarView=findViewById(R.id.getDate);
        dayReceive=findViewById(R.id.day_receive);
        dayReturn=findViewById(R.id.day_return);
        getDate();
    }
public void getDate(){
    //bo nhung ngay qua khu
    Calendar currentDate=Calendar.getInstance();


    List<Calendar> selectedDates = calendarView.getSelectedDates();
//        calendarView.getFirstSelectedDate();
    calendarView.setOnDayClickListener(new OnDayClickListener() {
        @Override
        public void onDayClick(EventDay eventDay) {
            Calendar clickedDayCalendar = eventDay.getCalendar();//Lay cai ngay dc chon
            pickCaculation(clickedDayCalendar);
            Log.d(TAG, "onDayClick: date= "+simpleDateFormat.format(clickedDayCalendar.getTime()));
        }
    });
}
public void pickCaculation(Calendar calendar){

//     if(calendar.compareTo()){
//              dayReceive.setText("Chon ngay ve");
//     }else{
//         dayReceive.setText(simpleDateFormat.format(calendar.getTime()));
//
//     }
}
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
