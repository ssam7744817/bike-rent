package com.example.bikerental.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.bikerental.R;

public class SumaryDialog extends DialogFragment {
    TextView bikeName;
    TextView bikeType;
//    Button accept, cancel;

    //Được dùng khi khởi tạo dialog mục đích nhận giá trị
    public static SumaryDialog newInstance() {
        SumaryDialog dialog = new SumaryDialog();
//        Bundle args = new Bundle();
//        args.putString("data", data);
//        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sumary, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // lấy giá trị tự bundle
//        String data = getArguments().getString("data", "");
//        BookingContract bookingPresenter=new BookingContract();
//        bikeName = (TextView) view.findViewById(R.id.bike_name_sumary);
//        bikeType = (TextView) view.findViewById(R.id.bike_type_sumary);
//        accept = (Button) view.findViewById(R.id.accept);
//        cancel = (Button) view.findViewById(R.id.cancel);
//        accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Thanh cong", Toast.LENGTH_SHORT).show();
//            }
//        });
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getDialog().dismiss();
//            }
//        });
    }

}