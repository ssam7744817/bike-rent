package com.example.bikerental.api.login;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.geocoding.GeocodingApiClient;
import com.example.bikerental.api.geocoding.GeocodingApiInterface;
import com.example.bikerental.api.model.geocoding.BikeLocation;
import com.example.bikerental.api.model.user.User;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginApiHome {
    private static final String TAG = "LoginApiClient";
    // api account
    private LoginInterface mApiAccount = LoginApiClient.getApiClient();
    public void login(String token,final ApiResponseResult callback){
        Call<User> call = mApiAccount.login(token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "login: success " );

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "login: me: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "login: me: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
