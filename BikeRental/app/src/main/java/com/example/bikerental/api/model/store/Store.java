
package com.example.bikerental.api.model.store;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Store {

    @SerializedName("scoure")
    @Expose
    private String scoure;
    @SerializedName("listBranches")
    @Expose
    private List<ListBranch> listBranches = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    public String getScoure() {
        return scoure;
    }

    public void setScoure(String scoure) {
        this.scoure = scoure;
    }

    public List<ListBranch> getListBranches() {
        return listBranches;
    }

    public void setListBranches(List<ListBranch> listBranches) {
        this.listBranches = listBranches;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
