package com.example.bikerental.api.road;

import android.util.Log;

import com.example.bikerental.Instance;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RoadAPIClient {
    private static String roadApiUrl="";
    private static RoadAPIInterface apiInterface;

    /**
     * create or return (if exists) connect to server
     *
     * @return: AccountApiInterface
     */
    public static RoadAPIInterface getApiClient() {
        if (apiInterface == null) {

            getServerUrl();

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(roadApiUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(RoadAPIInterface.class);
        }
        return apiInterface;
    }

    /**
     * get correct url for account api
     */
    private static void getServerUrl() {
        roadApiUrl = Instance.baseRoadUrl;
        Log.d("roadApiUrl", "getServerUrl: " + roadApiUrl);
    }
}
