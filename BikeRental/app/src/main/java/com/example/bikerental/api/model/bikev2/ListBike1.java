
package com.example.bikerental.api.model.bikev2;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListBike1 {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("bikeName")
    @Expose
    private String bikeName;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("bikeStatus")
    @Expose
    private Integer bikeStatus;
    @SerializedName("engineSize")
    @Expose
    private Integer engineSize;
    @SerializedName("transmissionType")
    @Expose
    private Integer transmissionType;
    @SerializedName("moneyRent")
    @Expose
    private Integer moneyRent;
    @SerializedName("moneyDeposit")
    @Expose
    private Integer moneyDeposit;
    @SerializedName("collaterals")
    @Expose
    private List<Integer> collaterals = null;
    @SerializedName("images")
    @Expose
    private List<String> images = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Integer getBikeStatus() {
        return bikeStatus;
    }

    public void setBikeStatus(Integer bikeStatus) {
        this.bikeStatus = bikeStatus;
    }

    public Integer getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(Integer engineSize) {
        this.engineSize = engineSize;
    }

    public Integer getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(Integer transmissionType) {
        this.transmissionType = transmissionType;
    }

    public Integer getMoneyRent() {
        return moneyRent;
    }

    public void setMoneyRent(Integer moneyRent) {
        this.moneyRent = moneyRent;
    }

    public Integer getMoneyDeposit() {
        return moneyDeposit;
    }

    public void setMoneyDeposit(Integer moneyDeposit) {
        this.moneyDeposit = moneyDeposit;
    }

    public List<Integer> getCollaterals() {
        return collaterals;
    }

    public void setCollaterals(List<Integer> collaterals) {
        this.collaterals = collaterals;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

}
