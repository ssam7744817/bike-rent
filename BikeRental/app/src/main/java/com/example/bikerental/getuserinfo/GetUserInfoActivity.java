package com.example.bikerental.getuserinfo;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.bikerental.FormatNumber;
import com.example.bikerental.GetDateActivity;
import com.example.bikerental.Instance;
import com.example.bikerental.MainActivity;
import com.example.bikerental.R;
import com.example.bikerental.api.booking.newbookingapi.Bookingv2;
import com.example.bikerental.contract.BookingContract;
import com.example.bikerental.dialog.SumaryDialog;
import com.example.bikerental.map.MapActivity;
import com.example.bikerental.presenter.BookingPresenter;
import com.example.bikerental.storelist.StoreListActivity;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class GetUserInfoActivity extends AppCompatActivity implements View.OnClickListener, BookingContract.SumaryView {

    ImageView hinh_xe;
    TextView loai_xe, dia_chi, total_price;
    Button accept, cancel, booking, toDay, fromDate;
    LinearLayout chooseDate, caculatingBox, chi_tiet;
    RadioGroup radioGroup;
    EditText phone;
    PlacesClient placesClient;
    TextView addressMySelf, storeAddress, originalPrice, tax, deposit, final_price,toAddress;
    String userAdress="", userLat, userLng, branchAddress;
    String branchId=Instance.bike.getBranch().getId();
    //    AutocompleteSupportFragment place_fragment;
    List<Place.Field> placeField = Arrays.asList(Place.Field.ID
            , Place.Field.NAME
            , Place.Field.ADDRESS);
    RadioButton buttonChose;
    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    AlertDialog dialog;
    String address = "";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    final String TAG = "SAM";
    final int REQUEST_CODE = 123;
    final int REQUEST_CODE_MAP = 234;
    final int REQUEST_CODE_CHOOSE_STORE = 345;
    BookingPresenter bookingPresenter;
    private int type=0;
    Bookingv2 bookingv2=new Bookingv2();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_user_info);
        Log.d(TAG, "onCreate: fdasdfadfas");
        init();
        initPlaceAutoComplete();
//        getValidToken();
    }

    /**
     * Thiết lập chức năng tự động gợi ý địa chỉ của google place api
     */
    private void initPlaceAutoComplete() {
        Places.initialize(this, Instance.API_KEY);
        placesClient = Places.createClient(this);
    }

    void init() {
        storeAddress = findViewById(R.id.address_by_your_self);
        storeAddress.setText(Instance.bike.getBranch().getAddress());
        addressMySelf = findViewById(R.id.addrres_my_self);
        loai_xe = findViewById(R.id.loai_xe);
        dia_chi = findViewById(R.id.dia_chi);
        total_price = findViewById(R.id.total_price);
        booking = findViewById(R.id.booking);
        radioGroup = findViewById(R.id.group_choose_bike);
        caculatingBox = findViewById(R.id.caculating_box);
        chi_tiet = findViewById(R.id.chi_tiet);
        originalPrice = findViewById(R.id.original_bike);
        tax = findViewById(R.id.tax);
        deposit = findViewById(R.id.deposit_detail);
        final_price = findViewById(R.id.final_price);
        bookingPresenter = new BookingPresenter(this, Instance.bike);
        phone=findViewById(R.id.getPhone);
        setBasicInfo();
        initOnClick();
        initRadioButtonClick();
    }


    private void initRadioButtonClick() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.tai_dai_li:
                        Toast.makeText(GetUserInfoActivity.this, "Tai dai li", Toast.LENGTH_SHORT).show();
                        group.clearCheck();
                        goToStoreListScreen();
                        break;
                    case R.id.tai_nha:
                        Toast.makeText(GetUserInfoActivity.this, "Tai nha", Toast.LENGTH_SHORT).show();
                        group.clearCheck();
                        goToChooseStoreScreen();
                        break;
                }
            }
        });


    }


    void initOnClick() {
//        chooseDate.setOnClickListener(this);
        booking.setOnClickListener(this);
//        toDay.setOnClickListener(this);
//        fromDate.setOnClickListener(this);
        chi_tiet.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.booking:
                /**
                 * O day se hien len 1 cai notification khi dat xe thanh cong
                 *
                 */
//                bookingPresenter.booking(Instance.bike.getId(),fromDate.getText().toString(),toDay.getText().toString(),type,branchId,userAdress);
                getUserInfo();
                bookingPresenter.booking(bookingv2);
                break;
//            case R.id.fromDate:
//                fromDate.setText("");//Lam the nay de chac chan se ko lay ngay cu de tinh toan
//                showDatePicker(R.id.fromDate);
//                break;
//            case R.id.toDate:
//                toDay.setText("");
//                showDatePicker(R.id.toDate);
//                break;
            case R.id.chi_tiet:

                break;
        }
    }

    @Override
    public void showSumaryDialog() {
        FragmentManager fm = getSupportFragmentManager();
        SumaryDialog userInfoDialog = SumaryDialog.newInstance();
        userInfoDialog.show(fm, null);
    }

    @Override
    public void goToChooseDateScreen() {
        startActivityForResult(new Intent(this, GetDateActivity.class), REQUEST_CODE);
    }

    @Override
    public void goToStoreListScreen() {
        startActivityForResult(new Intent(this, StoreListActivity.class), REQUEST_CODE_CHOOSE_STORE);
    }

    @Override
    public void goToChooseStoreScreen() {
        startActivityForResult(new Intent(this, MapActivity.class), REQUEST_CODE_MAP);
    }

    /**
     * Clear het back stack sau khi book thanh cong xe
     */
    @Override
    public void clearBackStack() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void showPrice() {
        int finalPrice = bookingPresenter.getFinaPrice();
        final_price.setText(FormatNumber.formatNumber(String.valueOf(finalPrice)));
        if (bookingPresenter.getDistance() != 0) {
            tax.setText(FormatNumber.formatNumber(String.valueOf(bookingPresenter.getDistance())));
        }
        Log.d(TAG, "showPrice: da vao roi");
    }

    @Override
    public void setBasicInfo() {
        deposit.setText(FormatNumber.formatNumber(Instance.bike.getMoneyDeposit().toString()));
        originalPrice.setText(FormatNumber.formatNumber(Instance.bike.getMoneyRent().toString()));
        tax.setText("0");

//
        int finalPrice = Instance.bike.getMoneyDeposit() + Instance.bike.getMoneyRent();
        final_price.setText(String.valueOf(finalPrice));
    }

    @Override
    public void showDatePicker(int textView) {
        Log.d("SAM", " đã vào pickDate: ");
        final Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int getDate = calendar.get(Calendar.DATE);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                if (textView == R.id.fromDate) {
                    fromDate.setText(simpleDateFormat.format(calendar.getTime()));
                    bookingPresenter.caculateDate(fromDate.getText().toString(), toDay.getText().toString());

                } else {
                    toDay.setText(simpleDateFormat.format(calendar.getTime()));
                    bookingPresenter.caculateDate(fromDate.getText().toString(), toDay.getText().toString());

                }
//                                    bookingPresenter.caculateDate(fromDate.getText().toString(),toDay.getText().toString());
            }
        }, year, month, getDate);
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE:

                // resultCode được set bởi DetailActivity
                // RESULT_OK chỉ ra rằng kết quả này đã thành công
                if (resultCode == Activity.RESULT_OK) {

                }
                break;
            case REQUEST_CODE_MAP:
                if (resultCode == Activity.RESULT_OK) {
                    //Nhan ket qua tra ve la 1 cai dia chi
                    address = data.getStringExtra(Instance.address);
                    int distance = data.getIntExtra(Instance.distance, 0);
                    addressMySelf.setText(address);
                    /**
                     * Lay thong tin ve vi tri cua nguoi dat xe
                     */
                    userAdress = data.getStringExtra(Instance.userAddress);
                    userLat = data.getStringExtra(Instance.userLat);
                    userLng = data.getStringExtra(Instance.userLng);
                    /**
                     * Ket thuc viec lay thong tin
                     */
                     type=1;
                    //Tinh toan gia tien neu option nay xay ra
                    bookingPresenter.setDistance(distance);
                    bookingPresenter.caculateDate(Instance.start_time, Instance.end_time);
                }
                break;
            case REQUEST_CODE_CHOOSE_STORE:
                if (resultCode == Activity.RESULT_OK) {
                    /**
                     * Chuan bi thong tin cua cua hang chon de den lay xe
                     */
                    branchAddress = data.getStringExtra(Instance.tai_cua_hang);
                    branchId = data.getStringExtra(Instance.branchID);
                    type=0;// set type
                    /**
                     * Ket thuc viec chuan bi
                     */
                    storeAddress.setText(branchAddress);
                }
                break;
        }
    }

    private void getUserInfo() {
        bookingv2.setBike(Instance.bike.getId());
        bookingv2.setPickUpDate(Instance.start_time);
        bookingv2.setReturnDate(Instance.end_time);
        bookingv2.setType(type);
        bookingv2.setBranchId(branchId);
        bookingv2.setReceiveAddress(userAdress);
        Log.d(TAG, "getUserInfo: User id= "+Instance.user.getId());
        /**
         * Lay user id tu shared preference
         */
        SharedPreferences sharedPreferences = this.getSharedPreferences(Instance.SharedToken, Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString(Instance.firebaseUserId, "");
        bookingv2.setUser(userId);
        bookingv2.setPhone(phone.getText().toString());
    }
//    private void getValidToken(){
//        /**
//         * Ko can den thu nay
//         */
//        SharedPreferences sharedPreferences = this.getSharedPreferences(Instance.SharedToken, Context.MODE_PRIVATE);
//        String token = sharedPreferences.getString(Instance.firebaseUserId, "");
////        bookingPresenter.getUserInfo(token);
//        /**
//         * User info se dc luu trong bien Instance.user
//         */
//    }
}
