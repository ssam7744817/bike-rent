package com.example.bikerental.api.road;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.model.road.Road;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;


import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RoadAPIHome {
    private static final String TAG = "RoadAPIClient";
    // api account
    private RoadAPIInterface mApiAccount = RoadAPIClient.getApiClient();
    public void getRoadInfo(String origins, String destinations, final ApiResponseResult callback){
        Call<Road> call = mApiAccount.getRoadInfo(Instance.UNITS,origins,destinations, Instance.API_KEY);
        call.enqueue(new Callback<Road>() {
            @Override
            public void onResponse(Response<Road> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "onResponse: getRoadInfo " + response.body().getRows().get(0).getElements().get(0).getDistance());

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "onResponse: getRoadInfo: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "onResponse getRoadInfo: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
