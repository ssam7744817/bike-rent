package com.example.bikerental.contract;

import com.google.firebase.auth.FirebaseAuth;

public class ProfileContract {
    public interface view {
        void signOut();

        void goToLogin();

        void showSuccessToast();

        void showInfo();

        void hideInfo();

        void showDialog();

        void hideDialog();

    }

    public interface presenter {

        void checkUserSignIn(FirebaseAuth auth);

        void checkUserToken(String token);

        void getIdToken(FirebaseAuth auth);

        //        void booking(Bookingv2 booking);


    }
}
