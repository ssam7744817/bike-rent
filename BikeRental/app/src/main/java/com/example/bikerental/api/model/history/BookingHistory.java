
package com.example.bikerental.api.model.history;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingHistory {

    @SerializedName("listBooking")
    @Expose
    private List<ListBooking> listBooking = null;
    @SerializedName("total")
    @Expose
    private Integer total;

    public List<ListBooking> getListBooking() {
        return listBooking;
    }

    public void setListBooking(List<ListBooking> listBooking) {
        this.listBooking = listBooking;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
