package com.example.bikerental.filter;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.adapter.BilkeAdapter;
import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.bikev2.BikeV2;
import com.example.bikerental.api.model.bikev2.ListBike1;
import com.example.bikerental.bikedetail.BikeDetailActivity;
import com.example.bikerental.contract.BikeListContract;
import com.example.bikerental.presenter.BikeListPresenter;
import com.example.bikerental.presenter.ListPresenter;

import dmax.dialog.SpotsDialog;

public class BikeListActivity extends AppCompatActivity implements BikeListContract.view {
    RecyclerView recyclerView;
    BilkeAdapter bilkeAdapter;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_list);
        recyclerView = findViewById(R.id.bike_list_recyele);
        ListPresenter listPresenter = new ListPresenter(this);
        dialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.get_data).build();
        listPresenter.getBikeList();
    }

    @Override
    public void setBikeToRecycleView(Bikes bike) {
        BikeListPresenter bikeListPresenter = new BikeListPresenter(bike);
        bilkeAdapter = new BilkeAdapter(bikeListPresenter, new BilkeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ListBike item) {
                Instance.bike = item;
                goToDetailScreen(item);
            }

        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(bilkeAdapter);
    }

    @Override
    public void goToDetailScreen(ListBike bike) {
//        Truyen object detail screen
        Log.d("SAM", "toAnotherScreen yes: ");
        Intent intent = new Intent(this, BikeDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void showDialog() {
        dialog.show();
    }

    @Override
    public void hideDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void showToast() {
        Toast.makeText(this, "List null", Toast.LENGTH_SHORT).show();
    }
}
