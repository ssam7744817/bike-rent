package com.example.bikerental.api.booking;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.bikes.BikesApiClient;
import com.example.bikerental.api.bikes.BikesApiInterface;
import com.example.bikerental.api.booking.newbookingapi.Bookingv2;
import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.booking.Booking;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BookingApiHome {
    private static final String TAG = "BookingApiClient";
    private BookingApiInterface mApiAccount = BookingApiClient.getApiClient();
//    public void booking(String bikeId,String fromDate,String toDate,int type,String branchId, String receivedId,final ApiResponseResult callback){
//        Call<Booking> call = mApiAccount.bookingABike(bikeId,"5de4907d23f52a07188b3ccb",fromDate,toDate,type,branchId,receivedId);
//        call.enqueue(new Callback<Booking>() {
//            @Override
//            public void onResponse(Response<Booking> response, Retrofit retrofit) {
//                if (response.isSuccess() && response.body() != null) {
//                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
//                    Log.d(TAG, "onResponse: booking " + response.body().getId());
//
//                } else {
//                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
//                    Log.d(TAG, "onResponse: booking: fail: " + response.code()+"message= "+response.message());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
//                Log.d(TAG, "onResponse booking: NETWORK_PROBLEM: " + t.toString());
//            }
//        });
//    }
    public void booking(Bookingv2 booking, final ApiResponseResult callback){
        Call<Booking> call = mApiAccount.bookingABike( booking);
        call.enqueue(new Callback<Booking>() {
            @Override
            public void onResponse(Response<Booking> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    Log.d(TAG, "onResponse: booking " + response.body().getId());

                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "onResponse: booking: fail: " + response.code()+"message= "+response.message());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "onResponse booking: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
