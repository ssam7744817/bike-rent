package com.example.bikerental.api.road;

import com.example.bikerental.api.model.road.Road;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RoadAPIInterface {
    @GET("json")
    Call<Road> getRoadInfo(
            @Query("units") String address,
            @Query("origins") String origins,
            @Query("destinations") String destinations,
            @Query("key") String key
    );
}
