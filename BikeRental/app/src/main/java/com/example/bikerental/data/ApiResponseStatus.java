package com.example.bikerental.data;

public enum ApiResponseStatus {
    SUCCESS,
    FAIL,
    NETWORK_PROBLEM;

    private ApiResponseStatus() {
    }
}
