package com.example.bikerental.presenter;

import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;

public class StoreListPresenter {
    private final Store storeList;

    public StoreListPresenter(Store bikeList) {
        this.storeList = bikeList;
    }

    public void onBindStoreRowViewAtPosition(int position, StoreListPresenter.StoreItemView rowView) {
        ListBranch bike = storeList.getListBranches().get(position);
        rowView.setAddressName(bike.getAddress());
rowView.setStoreName(bike.getBranchName());
    }


    public int getStoreItemCount() {
        return storeList.getListBranches().size();
    }

    public ListBranch getStore(int position) {
        return storeList.getListBranches().get(position);
    }

    public interface StoreItemView {

        void setAddressName(String name);
void setStoreName(String name);

    }
}
