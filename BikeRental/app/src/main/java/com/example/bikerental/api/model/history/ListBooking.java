
package com.example.bikerental.api.model.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListBooking {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("bike")
    @Expose
    private Bike bike;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("bookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("receiveAddress")
    @Expose
    private String receiveAddress;
    @SerializedName("pickUpDate")
    @Expose
    private String pickUpDate;
    @SerializedName("returnDate")
    @Expose
    private String returnDate;
    @SerializedName("returnAddress")
    @Expose
    private String returnAddress;
    @SerializedName("bookingStatus")
    @Expose
    private Integer bookingStatus;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("receiveType")
    @Expose
    private Integer receiveType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnAddress() {
        return returnAddress;
    }

    public void setReturnAddress(String returnAddress) {
        this.returnAddress = returnAddress;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(Integer receiveType) {
        this.receiveType = receiveType;
    }

}
