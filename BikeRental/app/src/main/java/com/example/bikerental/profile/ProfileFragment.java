package com.example.bikerental.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.api.model.user.User;
import com.example.bikerental.contract.ProfileContract;
import com.example.bikerental.login.LoginActivity;
import com.example.bikerental.presenter.ProfilePresenter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;


public class ProfileFragment extends Fragment implements ProfileContract.view, View.OnClickListener {

    LinearLayout signOut, userInfo;
    TextView userName, userPhone, userEmail;
    Button singIn;
    private GoogleSignInClient mGoogleSignInClient;
    final int RC_SIGN_IN = 456;
    ProfilePresenter profilePresenter;
    FirebaseAuth auth;
    AlertDialog dialog;
    CircleImageView imageView;
    private String TAG = "PROFILE";
    private User user = new User();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initLayout(view);
        profilePresenter = new ProfilePresenter(this);
        checkUserSignIn();
        return view;
    }

    private void initLayout(View view
    ) {
        dialog = new SpotsDialog.Builder().setContext(getActivity()).setTheme(R.style.get_data).build();
        userInfo = view.findViewById(R.id.linearLayout);
        singIn = view.findViewById(R.id.profile_dang_nhap);
        signOut = view.findViewById(R.id.sing_out);
        userName = view.findViewById(R.id.profile_user_name);
        userPhone = view.findViewById(R.id.user_phone);
        userEmail = view.findViewById(R.id.user_email_profile);
        auth = FirebaseAuth.getInstance();
        imageView = view.findViewById(R.id.profile_image);
        initOnClick();
    }

    /**
     * Kiem tra xem user co dang sign in ko, neu nhu ko dang nhap
     * thi getCurrentUser se null
     */
    private void checkUserSignIn() {
        profilePresenter.checkUserSignIn(auth);
    }

    private void initOnClick() {
        singIn.setOnClickListener(this);
        signOut.setOnClickListener(this);

    }

    /**
     * Config cho viec sign in bang google account
     *
     * @return
     */
    public GoogleSignInOptions initGoogleSignIn() {
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

    }

    @Override
    public void signOut() {
        //firebase sign out
        FirebaseAuth.getInstance().signOut();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), initGoogleSignIn());
        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(),
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        hideInfo();//Sau khi dang xuat xong thi giau het, chi chua lai cai dang ki
                        Toast.makeText(getActivity(), "Dang xuat thanh cong", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(getActivity().getBaseContext(), LoginActivity.class);
        intent.putExtra(Instance.profile, "profile");
        getActivity().startActivityForResult(intent, RC_SIGN_IN);
    }

    @Override
    public void showSuccessToast() {
        Toast.makeText(getActivity(), "Dang nhap thanh cong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInfo() {
        /**
         * Show ra nhung field thong tin
         */
        singIn.setVisibility(View.GONE);
        signOut.setVisibility(View.VISIBLE);
        userInfo.setVisibility(View.VISIBLE);
        user = profilePresenter.getUser();
        userName.setText(user.getFullname());
        Picasso.get().load(user.getAvatar()).into(imageView);

    }

    /**
     * Hien ra nhung field thong tin
     */
    @Override
    public void hideInfo() {
        hideDialog();// tat dialog
        signOut.setVisibility(View.GONE);
        userInfo.setVisibility(View.GONE);
        singIn.setVisibility(View.VISIBLE);
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.profile));
    }

    @Override
    public void showDialog() {
        dialog.show();
    }

    @Override
    public void hideDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_dang_nhap:
                goToLogin();
                break;
            case R.id.sing_out:
                signOut();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                user = (User) data.getSerializableExtra(Instance.USER);
                Log.d(TAG, "onActivityResult: Ve lai user profile roi");
                showInfo();
            }
        }else{
            Log.d(TAG, "onActivityResult: deo bang roi");
        }
    }
}
