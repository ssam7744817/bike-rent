package com.example.bikerental.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.api.model.geocoding.Geometry;
import com.example.bikerental.api.model.road.Road;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.contract.MapContract;
import com.example.bikerental.presenter.MapPresenter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, MapContract.View, EasyPermissions.PermissionCallbacks, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private GoogleMap mMap;
    PlacesClient placesClient;
    private MapPresenter mapPresenter;
    Button getCurrentLocation;
    private final String TAG = "SAM";
    private TextView time, distance;
    private AlertDialog waiting;
    private List<Marker> storeNearBy = new ArrayList<>();
    Road road;
    private String origin = "";

    //GoogleMap googleMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        waiting = new SpotsDialog.Builder().setContext(this).setTheme(R.style.get_data).build();
        initLayout();
        checkPermission();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initLayout() {
        Places.initialize(this, Instance.API_KEY);
        placesClient = Places.createClient(this);
        mapPresenter = new MapPresenter(placesClient, this);
        getCurrentLocation = findViewById(R.id.getCurrent);
        getCurrentLocation.setOnClickListener(this);
        time = findViewById(R.id.time);
        distance = findViewById(R.id.distance);

    }

    @AfterPermissionGranted(123)
    public void checkPermission() {
        /**
         * Huong dan de dang hoi quyen truy cap: https://www.youtube.com/watch?v=iHbdDAOJHIU
         */
        String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perm)) {
            //Neu nhu dong y

            Toast.makeText(this, "Ok nhe", Toast.LENGTH_SHORT).show();
        } else {
            //Neu nhu ko dong y
            EasyPermissions.requestPermissions(this, "We need permission ", 123, perm);
        }
//            }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getCurrent:
//                mapPresenter.cooldown(placesClient);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {

        }
    }

    /**
     * Bat truong hop click vao cai o thong tin cua marker, ta se lay cai dia chi do bo vao booking
     * quay tro lai getUserInfoActivity
     *
     * @param marker
     */
    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.d(TAG, "onInfoWindowClick: " + marker.getTitle());
        if (marker.getTitle() != null) {
            Intent intent = new Intent();
            intent.putExtra(Instance.address, marker.getTitle());// Lay dia chi cua cua hang de gui xe den vi tri hien tai
            String userAddres = mapPresenter.getAddressDetail().getResults().get(0).getFormattedAddress();
            String userLat = String.valueOf(mapPresenter.getAddressDetail().getResults().get(0).getGeometry().getLocation().getLat());
            String userLng = String.valueOf(mapPresenter.getAddressDetail().getResults().get(0).getGeometry().getLocation().getLng());
            intent.putExtra(Instance.userAddress, userAddres);// lay dia chi cua nguoi dat xe
            intent.putExtra(Instance.userLat, userLat);
            intent.putExtra(Instance.userLng, userLng);
            int distance = road.getRows().get(0).getElements().get(0).getDistance().getValue();
            intent.putExtra(Instance.distance, distance);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    /**
     * Tao ra cac marker tu nhung cua hang gan nha
     */

    @Override
    public void readyMarker() {
        if (mapPresenter.getStoreInDistrict().size() == 0) {
            showGPSProlem();
        }
        int count = 0;
        Log.d(TAG, "getStoreByDistrict: size" + mapPresenter.getStoreInDistrict().size());
        for (ListBranch branch : mapPresenter.getStoreInDistrict()) {
            LatLng latLng = new LatLng(Double.valueOf(branch.getLatitude()), Double.valueOf(branch.getLongtitude()));
            storeNearBy.add(mMap.addMarker(new MarkerOptions().position(latLng).title(branch.getAddress())));
            count++;
            if (count == mapPresenter.getStoreInDistrict().size()) {
                Log.d(TAG, "readyMarker: count dat dinh roi");
//                LatLng first = new LatLng(Double.valueOf(mapPresenter.getStoreInDistrict().get(0).getLatitude()), Double.valueOf(mapPresenter.getStoreInDistrict().get(0).getLongtitude()));
                //Lay vi tri hien tai cua nguoi dung

//                Geometry currentLocation = mapPresenter.getAddressDetail().getResults().get(0).getGeometry();
//                LatLng currentLatLng = new LatLng(currentLocation.getLocation().getLat(), currentLocation.getLocation().getLng());
                //Dua vi tri do vao marker custom
//                CircleOptions circleOptions = new CircleOptions().center(currentLatLng).radius(1000).strokeWidth(2).fillColor(R.color.colorPrimary_light_green).strokeColor(R.color.colorPrimary);
//                mMap.addMarker(new MarkerOptions().position(currentLatLng).title(mapPresenter.getAddressDetail().getResults().get(0).getFormattedAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//                mMap.addCircle(circleOptions);
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(first, 1));
//                moveCamera();
            }
        }
    }

    /**
     * Di chuyen cac camera den tat ca vi tri marker dc tao ra
     */
    @Override
    public void moveCamera() {
        final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                @Override
                public void onGlobalLayout() {
                    LatLngBounds.Builder bld = new LatLngBounds.Builder();
                    for (int i = 0; i < storeNearBy.size(); i++) {
                        LatLng ll = new LatLng(storeNearBy.get(i).getPosition().latitude, storeNearBy.get(i).getPosition().longitude);
                        bld.include(ll);
                    }

                    LatLngBounds bounds = bld.build();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 70));
                    mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                }
            });
        }
    }

    @Override
    public void showRoadInfo() {
        road = mapPresenter.getRoad();
        time.setText(road.getRows().get(0).getElements().get(0).getDuration().getText());
        distance.setText(road.getRows().get(0).getElements().get(0).getDistance().getText());
    }

    @Override
    public void finishScreen() {
        if (mapPresenter.getStoreInDistrict().size() == 0) {
            hideDialog();
            finish();
        } else {
            hideDialog();
        }
    }

    @Override
    public void showDialog() {
        Log.d(TAG, "showDialog: ");
        waiting.show();
        waiting.setCancelable(true);
        waiting.setCanceledOnTouchOutside(true);
    }

    @Override
    public void hideDialog() {
        Log.d(TAG, "hideDialog: ");
        if (waiting.isShowing()) {
            waiting.dismiss();
        }
    }

    @Override
    public void showGPSProlem() {
        Toast.makeText(this, "GPS da xay ra loi, dang thu lai....", Toast.LENGTH_SHORT).show();
    }

    /**
     * Cai nay se chay khi nhap vao marker cua hang, de lay khoang cach va thoi gian
     *
     * @param marker
     * @return
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
//        if (!marker.getId().equals("m3")) {
        Log.d(TAG, "onMarkerClick: id= " + marker.getId());
        marker.showInfoWindow();
        String destination = marker.getPosition().latitude + "," + marker.getPosition().longitude;
        mapPresenter.getRoadInfo(origin, destination);
        Log.d(TAG, "onMarkerClick: lat= " + marker.getPosition().latitude + " long= " + marker.getPosition().longitude);
//        } else {
//            marker.showInfoWindow();
//        }

        return true;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

        Log.d(TAG, "onMyLocationClick: ");
        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        origin = currentLatLng.latitude + "," + currentLatLng.longitude;
        mapPresenter.getLocationDetail(origin);
        Log.d(TAG, "onMyLocationClick: " + location.getLatitude() + " long= " + location.getLongitude());

    }

    @Override
    public boolean onMyLocationButtonClick() {

        return false;
    }
}
