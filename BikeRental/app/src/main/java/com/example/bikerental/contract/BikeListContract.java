package com.example.bikerental.contract;

import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.bikev2.BikeV2;
import com.example.bikerental.api.model.bikev2.ListBike1;

public class BikeListContract {
    public interface presenter{
        void getBikeList();
    }
    public interface view{
        void setBikeToRecycleView(Bikes bike);
        void goToDetailScreen(ListBike bike);
        void showDialog();
        void hideDialog();
        void showToast();
    }
}
