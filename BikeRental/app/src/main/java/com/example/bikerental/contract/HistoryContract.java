package com.example.bikerental.contract;

import com.example.bikerental.api.model.history.BookingHistory;

public class HistoryContract {
    public interface view{
        void showHistoryRecycleView(BookingHistory history);
        void showDialog();
        void hideDialog();
    }
    public interface presenter{
        void loadBookingHistory(String userId);
    }
}
