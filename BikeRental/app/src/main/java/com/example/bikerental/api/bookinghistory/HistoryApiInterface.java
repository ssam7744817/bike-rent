package com.example.bikerental.api.bookinghistory;

import com.example.bikerental.api.model.history.BookingHistory;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface HistoryApiInterface {
    @GET("bookings/user/{user_id}")
    Call<BookingHistory> getBookingHistory(
            @Path("user_id")String userid
    );
}
