package com.example.bikerental.api.booking.newbookingapi;

import java.io.Serializable;

public class Bookingv2 implements Serializable {
    private String bike,user,pickUpDate,returnDate,branchId,receiveAddress,phone;
    private int type;

    public String getBike() {
        return bike;
    }

    public String getBranchId() {
        return branchId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public void setBike(String bike) {
        this.bike = bike;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }


    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
