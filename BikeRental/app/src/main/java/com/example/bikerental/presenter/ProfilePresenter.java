package com.example.bikerental.presenter;

import androidx.annotation.NonNull;

import com.example.bikerental.api.login.LoginApiHome;
import com.example.bikerental.api.model.user.User;
import com.example.bikerental.contract.ProfileContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

public class ProfilePresenter implements ProfileContract.presenter {
    private String token;
    private User user;
    //    private String token="";
    private final String TAG = "SAM";
    private ProfileContract.view view;

    public ProfilePresenter(ProfileContract.view view) {
        this.view = view;
    }

    @Override
    public void checkUserSignIn(FirebaseAuth auth) {
        view.showDialog();
        if (auth.getCurrentUser() != null) {
            getIdToken(auth);//Neu nhu ko co null thi gui token den server cua app de kiem tra
        } else {
            view.hideInfo();
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Tra ve thong tin cua user neu nhu token okie,
     *
     * @param token
     */
    @Override
    public void checkUserToken(String token) {
        view.hideDialog();
        new LoginApiHome().login(token, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1) {
                    case SUCCESS:
                        setUser((User) var2);
                        view.showInfo();//Hien thong tin nguoi dung
                        break;
                    case FAIL:
                        break;
                    case NETWORK_PROBLEM:
                        break;
                }
            }
        });
    }

    /**
     * Ham nay dung de kiem tra xem cai token con dung dc hay ko
     *
     * @param auth
     */
    @Override
    public void getIdToken(FirebaseAuth auth) {
        if (auth.getCurrentUser() != null) {
            auth.getCurrentUser().getIdToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()) {
                        if (task.getResult().getToken() != null || !task.getResult().getToken().isEmpty()) {
                            setToken(task.getResult().getToken());
                            //Sau khi lay token xong, ta se kiem tra no
                            /**
                             * Neu nhu token ok, thi ta se lay thong tin ma token co
                             * con ko thi thoi
                             */
                            checkUserToken(getToken());
                        }
                    }
                }
            });
        }
    }

//    @Override
//    public void getUserInfo(String token) {
//
//    }


    /**
     * Lay thong tin nguoi dung tu token cua firebase tra ve
     *
     * @param
     */
//    @Override
//    public void getUserInfo(String token) {
//        new LoginApiHome().login(token, new ApiResponseResult() {
//            @Override
//            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
//                switch (var1){
//                    case NETWORK_PROBLEM:
//                        Log.d(TAG, "onResponse: getUserInfo Net");
//                        break;
//                    case FAIL:
//                        Log.d(TAG, "onResponse: getUserInfo FAIL");
//                        break;
//                    case SUCCESS:
//                        Log.d(TAG, "onResponse: getUserInfo SUCCESS");
//                        Instance.user=(User)var2;
//                        Log.d(TAG, "onResponse: user id= "+Instance.user.getId());
//                        break;
//                }
//            }
//        });
//
//    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
