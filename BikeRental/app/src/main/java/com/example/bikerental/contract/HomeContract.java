package com.example.bikerental.contract;

public class HomeContract {
    public interface presenter {
//        void getBikeList();
    }

    public interface view {
        //        void setBikeToRecycleView(Bikes bike);
//        void goToDetailScreen(ListBike bike);
//        void showDialog();
//        void hideDialog();
        void showDatePicker(int widgetName);
        void getReadyForFindBike();
        void changeTayGaColor();
        void changeXeSoColor();
    }

}
