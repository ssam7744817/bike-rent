package com.example.bikerental;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.bikerental.login.LoginActivity;

public class ChooseTypeOfSignInActivity extends AppCompatActivity implements View.OnClickListener {
private Button haveAccount,noAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_type_of_sign_in);
        initLayout();
    }
    private void initLayout(){
        haveAccount=findViewById(R.id.have_account);
        noAccount=findViewById(R.id.already_have_account);
        initOnClick();
    }
    private void initOnClick(){
        haveAccount.setOnClickListener(this);
        noAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id =v.getId();
        switch (id){
            case R.id.have_account:
startActivity(new Intent(this,LoginActivity.class));
                break;
            case R.id.already_have_account:
                startActivity(new Intent(this, SignInActivity.class));
                break;
        }
    }
}
