package com.example.bikerental.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.bikerental.R;
import com.example.bikerental.contract.BookingContract;

public class PriceDetailDialog extends DialogFragment {
    TextView original,tax,finalPrice,deposit;

    //Được dùng khi khởi tạo dialog mục đích nhận giá trị
    public static PriceDetailDialog newInstance() {
        PriceDetailDialog dialog = new PriceDetailDialog();
//        Bundle args = new Bundle();
//        args.putString("data", data);
//        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.caculating_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // lấy giá trị tự bundle
//        String data = getArguments().getString("data", "");
        BookingContract bookingContract =new BookingContract();
        original = (TextView) view.findViewById(R.id.original_bike);
        tax = (TextView) view.findViewById(R.id.tax);
        finalPrice = (TextView) view.findViewById(R.id.final_price);
        deposit = (TextView) view.findViewById(R.id.deposit);

    }
}
