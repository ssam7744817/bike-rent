package com.example.bikerental.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.Constants;
import com.example.bikerental.R;
import com.example.bikerental.api.model.history.BookingHistory;
import com.example.bikerental.api.model.history.ListBooking;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyBookingAdapter extends RecyclerView.Adapter<MyBookingAdapter.MyBookingHolder> {
    BookingHistory bookingHistory;
    private static final String TAG = "SAM";
    public MyBookingAdapter(BookingHistory booking) {
        this.bookingHistory = booking;
    }

    @NonNull
    @Override
    public MyBookingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_bike_item, parent, false);
        return new MyBookingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBookingHolder holder, int position) {
        List<ListBooking> listOfBooking = bookingHistory.getListBooking();
        ListBooking booking = listOfBooking.get(position);
        Log.d(TAG, "onBindViewHolder: bike name"+booking.getBike().getBikeName());
        String name=booking.getBike().getBikeName();
        holder.bikeName.setText(name);
        holder.bikeType.setText(booking.getBike().getTransmissionType().toString());
        if(booking.getBike().getTransmissionType()==0){
            holder.bikeType.setText("Xe xo");
        }else{
            holder.bikeType.setText("Xe tay ga");
        }
        holder.pickUpDate.setText(booking.getPickUpDate());
        holder.dropOffDate.setText(booking.getReturnDate());
        holder.diaChiaNhanXe.setText(booking.getReturnAddress());
        if (booking.getReceiveType() == 0) {
            holder.kieuNhanXe.setText("Nhan xe tai dai ly");
        } else {
            holder.kieuNhanXe.setText("Nhan xe tai vi tri hien tai");
        }
        switch (booking.getBookingStatus()) {
            case 0:
                holder.status.setText("WAITTING");
                break;
            case 1:
                holder.status.setText("ACCEPTED");
                break;
            case 2:
                holder.status.setText("USING");
                break;
            case 3:
                holder.status.setText("SUCCESS");
                break;
            case 4:
                holder.status.setText("CANCEL");
                break;
        }
            Picasso.get().load(booking.getBike().getImages().get(0)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return bookingHistory.getListBooking().size();
    }

    public class MyBookingHolder extends RecyclerView.ViewHolder {
        TextView pickUpDate, dropOffDate, bikeName, bikeType, status, kieuNhanXe, diaChiaNhanXe;
        ImageView imageView;

        public MyBookingHolder(@NonNull View itemView) {
            super(itemView);
            pickUpDate = itemView.findViewById(R.id.pick_up_date);
            dropOffDate = itemView.findViewById(R.id.drop_off_date);
            bikeName = itemView.findViewById(R.id.bike_name_history);
            bikeType = itemView.findViewById(R.id.bikeType);
            status = itemView.findViewById(R.id.status);
            kieuNhanXe = itemView.findViewById(R.id.loai_xe_my_bike);
            diaChiaNhanXe = itemView.findViewById(R.id.dia_chi_my_bike);
            imageView=itemView.findViewById(R.id.image_history);
        }
    }
}
