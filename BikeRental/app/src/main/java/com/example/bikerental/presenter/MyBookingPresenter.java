package com.example.bikerental.presenter;

import com.example.bikerental.api.model.bike.ListBike;

import java.util.List;

public class MyBookingPresenter {
    private final List<ListBike> bikeList;

    public MyBookingPresenter(List<ListBike> bikeList) {
        this.bikeList = bikeList;
    }

    public void onBindBikeRowViewAtPosition(int position, BikeListPresenter.BikeItemView rowView) {
        ListBike bike = bikeList.get(position);
        rowView.setName(bike.getBikeName());
        rowView.setPrice(String.valueOf(bike.getMoneyRent()));
//        rowView.setImage(Integer.valueOf(bike.getImages().get(0)));
    }


    public int getBikeItemCount() {
        return bikeList.size();
    }

    public ListBike getBike(int position) {
        return bikeList.get(position);
    }

    public interface BikeItemView {

        void setName(String name);

        void setPrice(String price);

        void setImage(int image);


    }
}
