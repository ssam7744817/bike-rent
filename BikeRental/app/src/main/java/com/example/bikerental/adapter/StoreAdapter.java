package com.example.bikerental.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.R;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.presenter.BikeListPresenter;
import com.example.bikerental.presenter.StoreListPresenter;
import com.squareup.picasso.Picasso;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreHolder> {
    private final StoreListPresenter presenter;
    private final StoreAdapter.OnItemClickListener listener;//bắt sự kiện click
    private final String TAG = "SAM";

    public StoreAdapter(StoreListPresenter presenter, StoreAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.presenter = presenter;
    }

    public interface OnItemClickListener {
        //        void onItemClick(Request item);
        void onItemClick(ListBranch item);
    }

    @NonNull
    @Override
    public StoreAdapter.StoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StoreHolder view = new StoreHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.store_item, parent, false));
        return view;
    }

    @Override
    public void onBindViewHolder(@NonNull StoreAdapter.StoreHolder holder, int position) {
        presenter.onBindStoreRowViewAtPosition(position, holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Adpter đã nhận ");
               listener.onItemClick(presenter.getStore(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return presenter.getStoreItemCount();
    }

    public class StoreHolder extends RecyclerView.ViewHolder implements StoreListPresenter.StoreItemView {
        TextView storeAddress,store_name;

        public StoreHolder(@NonNull View itemView) {
            super(itemView);
            store_name=itemView.findViewById(R.id.store_name);
            storeAddress=itemView.findViewById(R.id.store_item_address);
        }

        @Override
        public void setAddressName(String name) {
storeAddress.setText(name);
        }

        @Override
        public void setStoreName(String name) {
            store_name.setText(name);
        }
    }
}
