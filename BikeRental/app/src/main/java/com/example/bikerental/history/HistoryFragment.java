package com.example.bikerental.history;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.adapter.MyBookingAdapter;
import com.example.bikerental.api.model.history.BookingHistory;
import com.example.bikerental.contract.HistoryContract;
import com.example.bikerental.presenter.HistoryPresenter;

import dmax.dialog.SpotsDialog;


public class HistoryFragment extends Fragment implements HistoryContract.view {
    RecyclerView historyRecycView;
    MyBookingAdapter myBookingAdapter;
    HistoryPresenter historyPresenter = new HistoryPresenter(this);
    private AlertDialog dialog;
    private final String TAG = "SAM";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layoutInflater = inflater.inflate(R.layout.fragment_history, container, false);
        historyRecycView = layoutInflater.findViewById(R.id.history_recyclew_view);
        dialog = new SpotsDialog.Builder().setContext(getActivity()).setTheme(R.style.get_data).build();
        readyListHistoryBooking();
        return layoutInflater;
    }


    public void readyListHistoryBooking() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Instance.SharedToken, Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString(Instance.firebaseUserId, "");
        Log.d(TAG, "readyListHistoryBooking: user id= " + userId);
        historyPresenter.loadBookingHistory(userId);
    }

    @Override
    public void showHistoryRecycleView(BookingHistory history) {
        myBookingAdapter = new MyBookingAdapter(history);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        historyRecycView.setLayoutManager(layoutManager);
        historyRecycView.setAdapter(myBookingAdapter);
    }

    @Override
    public void showDialog() {
        dialog.show();
    }

    @Override
    public void hideDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
