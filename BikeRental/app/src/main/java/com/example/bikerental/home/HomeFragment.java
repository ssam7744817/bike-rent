package com.example.bikerental.home;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.adapter.BilkeAdapter;
import com.example.bikerental.contract.HomeContract;
import com.example.bikerental.filter.BikeListActivity;
import com.example.bikerental.presenter.HomePresenter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class HomeFragment extends Fragment implements HomeContract.view, View.OnClickListener {
    //    @BindView(R.id.suzuki_view)
    RecyclerView suzuki_view;
    BilkeAdapter bilkeAdapter;
    private String TAG = "SAM";
    HomePresenter home;
    private AlertDialog dialog;
    private Button fromDate, toDate, tay_ga, tay_so, next,changeColor;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private int loai_xe = 0;

    // 0: xe xo, 1: xe tay ga
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        initLayout(root);
        return root;

    }

    //
    private void initLayout(View root) {
        fromDate = root.findViewById(R.id.fromDate_first);
        toDate = root.findViewById(R.id.toDate_first);
        tay_ga = root.findViewById(R.id.xe_tay_ga);
        tay_so = root.findViewById(R.id.xe_so);
        next = root.findViewById(R.id.finish_list_choose);
        // set
        tay_ga.setOnClickListener(this);
        tay_so.setOnClickListener(this);
        fromDate.setOnClickListener(this);
        toDate.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void showDatePicker(int textView) {
        Log.d("SAM", " đã vào pickDate: ");
        final Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int getDate = calendar.get(Calendar.DATE);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                if (textView == R.id.fromDate_first) {
                    fromDate.setText(simpleDateFormat.format(calendar.getTime()));
                } else {
                    toDate.setText(simpleDateFormat.format(calendar.getTime()));

                }
//                                    bookingPresenter.caculateDate(fromDate.getText().toString(),toDay.getText().toString());
            }
        }, year, month, getDate);
        datePickerDialog.show();
    }

    @Override
    public void getReadyForFindBike() {
//        Intent intent = new Intent(getActivity(), BikeListActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putInt(Instance.loai_xe, loai_xe);
//        bundle.putString(Instance.start_time, fromDate.getText().toString());
//        bundle.putString(Instance.end_time, toDate.getText().toString());
        Instance.loai_xe=loai_xe;
        Instance.start_time=fromDate.getText().toString();
        Instance.end_time=toDate.getText().toString();
       startActivity(new Intent(getActivity(),BikeListActivity.class));
    }

    @Override
    public void changeTayGaColor() {
        tay_ga.setBackground(getActivity().getDrawable(R.color.colorPrimary));
        tay_ga.setTextColor(getActivity().getResources().getColor(R.color.white));
        tay_so.setTextColor(getActivity().getResources().getColor(R.color.black));
        tay_so.setBackground(getActivity().getDrawable(R.color.grey));
    }

    @Override
    public void changeXeSoColor() {
        tay_so.setBackground(getActivity().getDrawable(R.color.colorPrimary));
        tay_so.setTextColor(getActivity().getResources().getColor(R.color.white));
        tay_ga.setTextColor(getActivity().getResources().getColor(R.color.black));
        tay_ga.setBackground(getActivity().getDrawable(R.color.grey));
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.xe_tay_ga:
                loai_xe = 1;
                changeTayGaColor();
                break;
            case R.id.xe_so:
                changeXeSoColor();
                break;
            case R.id.fromDate_first:
                fromDate.setText("");
                showDatePicker(R.id.fromDate_first);
                break;
            case R.id.toDate_first:
                toDate.setText("");
                showDatePicker(R.id.toDate_first);
                break;
            case R.id.finish_list_choose:
                getReadyForFindBike();
                break;
        }
    }
//
//
//    @Override
//    public void setBikeToRecycleView(Bikes bike) {
//
//        BikeListPresenter bikeListPresenter = new BikeListPresenter(bike);
//        bilkeAdapter = new BilkeAdapter(bikeListPresenter, new BilkeAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(ListBike item) {
//                Instance.bike = item;
//                goToDetailScreen(item);
//            }
//
//        });
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        suzuki_view.setLayoutManager(layoutManager);
//        suzuki_view.setAdapter(bilkeAdapter);
//    }
//
//    @Override
//    public void goToDetailScreen(ListBike bike) {
//        //Truyen object detail screen
//        Log.d("SAM", "toAnotherScreen yes: ");
//        Intent intent = new Intent(getActivity().getBaseContext(), BikeDetailActivity.class);
//        getActivity().startActivity(intent);
//    }
//
//    @Override
//    public void showDialog() {
//        dialog.show();
//    }
//
//    @Override
//    public void hideDialog() {
//        if (dialog.isShowing()) {
//            dialog.dismiss();
//        }
//    }
}