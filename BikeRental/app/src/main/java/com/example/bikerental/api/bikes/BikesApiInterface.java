package com.example.bikerental.api.bikes;

import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bikev2.BikeV2;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface BikesApiInterface {
    @GET("bikes/")
    Call<Bikes> getBikes(@Query("pickUpDate")String pickUpDate,
                          @Query("returnDate")String returnDate,
                          @Query("transmissionType")String transmissionType
    );
}
