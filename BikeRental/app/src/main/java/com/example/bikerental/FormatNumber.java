package com.example.bikerental;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class FormatNumber {
    public static String formatNumber(String s) {
        try {
            String originalString = s.toString();

            Long longval;
            if (originalString.contains(",")) {
                originalString = originalString.replaceAll(",", "");
            }
            longval = Long.parseLong(originalString);

            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###,###,###");
            s= formatter.format(longval);
            //setting text after format to EditText
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
return s;
    }

}
