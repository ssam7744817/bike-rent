package com.example.bikerental.presenter;


import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.bikev2.BikeV2;
import com.example.bikerental.api.model.bikev2.ListBike1;

public class BikeListPresenter {
    private final Bikes bikeList;

    public BikeListPresenter(Bikes bikeList) {
        this.bikeList = bikeList;
    }

    public void onBindBikeRowViewAtPosition(int position, BikeItemView rowView) {
        ListBike bike = bikeList.getListBike().get(position);
        rowView.setName(bike.getBikeName());
        rowView.setRent(String.valueOf(bike.getMoneyRent()));
        rowView.setPrice(String.valueOf(bike.getMoneyRent()));
        rowView.setImage((bike.getImages().get(0)));
    }


    public int getBikeItemCount() {
        return bikeList.getListBike().size();
    }

    public ListBike getBike(int position) {
        return bikeList.getListBike().get(position);
    }

    public interface BikeItemView {

        void setName(String name);

        void setPrice(String price);

        void setImage(String image);
void setRent(String rent);

    }

}
