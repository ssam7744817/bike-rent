package com.example.bikerental.contract;

public class MainActivityContract {
   public interface MainView{
//         void signOut();
        void goToProfile();
       void setAvatarAndName(String name,String u);
    }
    public interface MainPresenter {
        void checkTokenIsExpired();

    }
}
