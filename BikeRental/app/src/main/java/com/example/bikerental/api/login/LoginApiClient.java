package com.example.bikerental.api.login;

import com.example.bikerental.Instance;
import com.example.bikerental.api.geocoding.GeocodingApiInterface;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class LoginApiClient {
    private static String loginApiUrl="";
    private static LoginInterface apiInterface;

    /**
     * create or return (if exists) connect to server
     *
     * @return: AccountApiInterface
     */
    public static LoginInterface getApiClient() {
        if (apiInterface == null) {

            getServerUrl();

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(loginApiUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(LoginInterface.class);
        }
        return apiInterface;
    }

    /**
     * get correct url for account api
     */
    private static void getServerUrl() {
        loginApiUrl = Instance.BikeUrl;
//        Log.d("accountBaseUrl", "getServerUrl: " + accountBaseUrl);
    }
}
