
package com.example.bikerental.api.model.bike;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bikes {

    @SerializedName("listBike")
    @Expose
    private List<ListBike> listBike = null;
    @SerializedName("total")
    @Expose
    private Integer total;

    public List<ListBike> getListBike() {
        return listBike;
    }

    public void setListBike(List<ListBike> listBike) {
        this.listBike = listBike;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
