package com.example.bikerental.contract;

import com.example.bikerental.api.model.store.Store;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.libraries.places.api.net.PlacesClient;

public class MapContract  {
    public interface Presenter {
        void getCurrentPlace(PlacesClient placesClient);
        void getLocationDetail(String origin);
        void getAllStore();
        void getStoreByDistrict(String district);
        void getRoadInfo(String origin,String destination);
        void cooldown(String orgin);
    }
    public interface View{
        void readyMarker();
        void moveCamera();
        void showRoadInfo();
        void finishScreen();
        void showDialog();
        void hideDialog();
        void showGPSProlem();
    }
}
