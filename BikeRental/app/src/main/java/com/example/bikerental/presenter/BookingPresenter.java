package com.example.bikerental.presenter;

import android.os.CountDownTimer;
import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.booking.BookingApiHome;
import com.example.bikerental.api.booking.newbookingapi.Bookingv2;
import com.example.bikerental.api.login.LoginApiHome;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.booking.Booking;
import com.example.bikerental.api.model.user.User;
import com.example.bikerental.contract.BookingContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

public class BookingPresenter implements BookingContract.presenter {
    private ListBike bike;
    private int driveTax;
    private int distance;
    private int finaPrice;
    private int date;
    private final String TAG="SAM";
    private BookingContract.SumaryView view;
    private final static int MAXIMUM_TIME = 10000;
    private final static int COUNTDOWN_TIME = 1000;
    public BookingPresenter(BookingContract.SumaryView view,ListBike bike){
        this.bike=bike;
        this.view=view;
    }


    @Override
    public void checkNull(String bikeId, String fromDate, String toDate, String branchId, String userAddress) {
        if(toDate.isEmpty()){

        }else if(fromDate.isEmpty()){

        }else if(branchId.isEmpty()&&userAddress.isEmpty()){

        }
    }

    @Override
    public void caculateDate(String fromDate,String toDate) {
        if(fromDate!=null && toDate!=null){
            if(!fromDate.isEmpty()&&!toDate.isEmpty()){
                String start=fromDate.substring(0,2);
                String end=toDate.substring(0,2);
                int totalDate=Integer.valueOf(end)-Integer.valueOf(start);
                Log.d(TAG, "start: "+start+" end: "+end);
                Log.d(TAG, "\n"+"from date= "+fromDate+"\n"+"to date= "+toDate+"\n"+"caculateDate: total= "+totalDate);
                setDate(totalDate);
                caculatePrice();
            }
        }

    }

    @Override
    public void caculatePrice() {
        if(distance!=0){
            setFinaPrice(bike.getMoneyDeposit()+bike.getMoneyRent()*getDate()+getDistance());
            Log.d(TAG, "caculatePrice: final price= "+getFinaPrice());
        }else{
            setFinaPrice(bike.getMoneyDeposit()+bike.getMoneyRent()*getDate());
            Log.d(TAG, "caculatePrice: final price= "+getFinaPrice());
        }
        view.showPrice();
    }
//
//    /**
//     * Lay thong tin nguoi dung tu token cua firebase tra ve
//     * @param token
//     */
//    @Override
//    public void getUserInfo(String token) {
//                new LoginApiHome().login(token, new ApiResponseResult() {
//                    @Override
//                    public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
//                        switch (var1){
//                            case NETWORK_PROBLEM:
//                                Log.d(TAG, "onResponse: getUserInfo Net");
//                                break;
//                            case FAIL:
//                                Log.d(TAG, "onResponse: getUserInfo FAIL");
//                                break;
//                            case SUCCESS:
//                                Log.d(TAG, "onResponse: getUserInfo SUCCESS");
//                                Instance.user=(User)var2;
//                                Log.d(TAG, "onResponse: user id= "+Instance.user.getId());
//                                break;
//                        }
//                    }
//                });
//
//    }

//    @Override
//    public void booking(String bikeId, String fromDate, String toDate, int type, String branchId, String address) {
//        Log.d(TAG, "booking:"+"\n"+"bikeId= "+bikeId+"\n"+"from date= "+fromDate+"\n"+"to date= "+toDate+"\n"+"type= "+type+"\n"+"branchId= "+branchId+"\n"+"address= "+address);
//        new BookingApiHome().booking(bikeId, fromDate, toDate, type, branchId, address, new ApiResponseResult() {
//            @Override
//            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
//                switch (var1){
//                    case SUCCESS:
//                        /**
//                         * Thanh cong thi hien thong bao va quay ve man hinh home
//                         */
//                        Log.d(TAG, "onResponse booking: SUCCESS");
//                        view.showSumaryDialog();
//                        view.clearBackStack();
//                        break;
//                    case FAIL:
//                        Log.d(TAG, "onResponse booking: FAIL");
//                        break;
//                    case NETWORK_PROBLEM:
//                        Log.d(TAG, "onResponse booking: NET");
//                        break;
//                }
//            }
//        });
//    }
    @Override
    public void booking(Bookingv2 booking) {
//        Log.d(TAG, "booking:"+"\n"+"bikeId= "+bikeId+"\n"+"from date= "+fromDate+"\n"+"to date= "+toDate+"\n"+"type= "+type+"\n"+"branchId= "+branchId+"\n"+"address= "+address);
        Log.d(TAG, "booking:"+"\n"+"user Id: "+booking.getUser()+"\n"+"bikeId= "+booking.getBike()+"\n"+"from date= "+booking.getPickUpDate()+"\n"+"to date= "+booking.getReturnDate()+"\n"+"type= "+booking.getType()+"\n"+"branchId= "+booking.getBranchId()+"\n"+"address= "+booking.getReceiveAddress()+"\n"+"phone= "+booking.getPhone());
        new BookingApiHome().booking(booking, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                switch (var1){
                    case SUCCESS:
                        /**
                         * Thanh cong thi hien thong bao va quay ve man hinh home
                         */
                        Log.d(TAG, "onResponse booking: SUCCESS");
                        view.showSumaryDialog();
                        cooldown();
                        break;
                    case FAIL:
                        Log.d(TAG, "onResponse booking: FAIL");
                        break;
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "onResponse booking: NET");
                        break;
                }
            }
        });
    }

    @Override
    public void cooldown() {
        /**
         * this method help you make a coutdown for resend code when SMS timeout
         * the SMS time out will be set in sendVerificationCode()
         */

        new CountDownTimer(MAXIMUM_TIME, COUNTDOWN_TIME) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                view.clearBackStack();
            }
        }.start();

    }

    public int getDriveTax() {
        return driveTax;
    }

    public void setDriveTax(int driveTax) {
        this.driveTax = driveTax;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getFinaPrice() {
        return finaPrice;
    }

    public void setFinaPrice(int finaPrice) {
        this.finaPrice = finaPrice;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance/1000*5000;
    }
}
