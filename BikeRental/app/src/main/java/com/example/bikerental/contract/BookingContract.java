package com.example.bikerental.contract;

import com.example.bikerental.api.booking.newbookingapi.Bookingv2;
import com.example.bikerental.api.model.booking.Booking;

public class BookingContract {
    public interface presenter {
        void checkNull(String bikeId,String fromDate,String toDate,String branchId,String userAddress);

        void caculateDate(String fromDate, String toDate);

        void caculatePrice();

//        void getUserInfo(String token);

//        void booking(String bikeId, String fromDate, String toDate, int type, String branchId, String address);
        void booking(Bookingv2 booking);
        void cooldown();
    }

    public interface SumaryView {
        void showSumaryDialog();

        void goToChooseDateScreen();

        void goToStoreListScreen();

        void goToChooseStoreScreen();

        void clearBackStack();

        void showPrice();

        void setBasicInfo();

        void showDatePicker(int textViewName);

    }
}
