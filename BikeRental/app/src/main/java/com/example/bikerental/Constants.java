package com.example.bikerental;

public class Constants {
    public interface BOOKINGSTATUS {
        public static int WAITTING = 0;
        public static int ACCEPTED = 1;
        public static int USING = 2;
        public static int SUCCESS = 3;
        public static int CANCEL = 4;
    }

    public interface BIKESTATUS {
        public static int AVAILABLE = 0;
        public static int USING = 1;
        public static int DISABLE = 2;
    }
}
