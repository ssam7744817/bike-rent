package com.example.bikerental.api.store;

import android.util.Log;

import com.example.bikerental.Instance;
import com.example.bikerental.api.road.RoadAPIInterface;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class StoreApiClient {
    private static String StoreApiUrl = "";
    private static StoreApiInterface apiInterface;

    /**
     * create or return (if exists) connect to server
     *
     * @return: AccountApiInterface
     */
    public static StoreApiInterface getApiClient() {
        if (apiInterface == null) {

            getServerUrl();

            OkHttpClient okClient = new OkHttpClient();
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(StoreApiUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(StoreApiInterface.class);
        }
        return apiInterface;
    }

    /**
     * get correct url for account api
     */
    private static void getServerUrl() {
        StoreApiUrl = Instance.storeUrl;
        Log.d("roadApiUrl", "getServerUrl: " + StoreApiUrl);
    }
}
