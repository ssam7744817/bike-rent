package com.example.bikerental.data;

public interface ApiResponseResult {
    void onResponse(ApiResponseStatus var1, Object var2, int var3);
}
