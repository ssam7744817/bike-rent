package com.example.bikerental.api.bikes;

import android.util.Log;

import com.example.bikerental.api.model.bike.Bikes;
import com.example.bikerental.api.model.bikev2.BikeV2;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BikesApiHome {
    private static final String TAG = "BikesApiClient";
    // api account
    private BikesApiInterface mApiAccount = BikesApiClient.getApiClient();
    public void getBikeList(String pickUpDate,String returnDate,String transmissionType,final ApiResponseResult callback){
        Call<Bikes> call = mApiAccount.getBikes(pickUpDate,returnDate,transmissionType);
        call.enqueue(new Callback<Bikes>() {
            @Override
            public void onResponse(Response<Bikes> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body() != null) {
                    callback.onResponse(ApiResponseStatus.SUCCESS, response.body(), response.code());
                    if(response.body().getListBike().size()>0){
                        Log.d(TAG, "onResponse: getBikeList " + response.body().getListBike().get(0).getBikeName());
                    }
                } else {
                    callback.onResponse(ApiResponseStatus.FAIL, null, response.code());
                    Log.d(TAG, "onResponse: getBikeList: fail: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onResponse(ApiResponseStatus.NETWORK_PROBLEM, null, -1);
                Log.d(TAG, "onResponse getBikeList: NETWORK_PROBLEM: " + t.toString());
            }
        });
    }
}
