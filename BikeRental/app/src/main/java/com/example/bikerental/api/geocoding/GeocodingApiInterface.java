package com.example.bikerental.api.geocoding;

import com.example.bikerental.api.model.geocoding.BikeLocation;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface GeocodingApiInterface {
    @GET("json")
    Call<BikeLocation> getLocation(
            @Query("address") String place_id,
            @Query("key") String apiKey

    );
}
