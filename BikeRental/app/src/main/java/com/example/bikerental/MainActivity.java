package com.example.bikerental;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.bikerental.contract.MainActivityContract;
import com.example.bikerental.history.HistoryFragment;
import com.example.bikerental.home.HomeFragment;
import com.example.bikerental.profile.ProfileFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainActivity extends AppCompatActivity implements  MainActivityContract.MainView {

    private AppBarConfiguration mAppBarConfiguration;
    private ImageView avatar;
    private TextView avatar_name;
    private String TAG = "SAM";
    DrawerLayout drawer;
    NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLayout();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.nav_profile,R.id.nav_history)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
getCurentToken();
    }

    private void initLayout() {
        avatar=findViewById(R.id.imageView_navigation);
        avatar_name=findViewById(R.id.user_name_navigation);
    }



    /**
     * Lay cai curren token hien tai
     * Ham nay dung de test
     */
    public void getCurentToken(){
    FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    Log.d(TAG, "onComplete: id gif do= "+token);
                    // Log and toast
                    String msg = token;
                    Log.d(TAG, msg);
//                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            });
}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * Ham dung de thay fragment moi khi chon tren navigation drawer
     * @param itemId
     */
    private void displaySelectdScreen(MenuItem itemn,int itemId) {
        Log.d(TAG, "back stack count: "+  getSupportFragmentManager().getBackStackEntryCount());
String tittle="";
String fragmentName="";
        //creating fragment object
        Fragment fragment = null;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        //initializing the fragment object which is selected
        switch (itemId) {

            case R.id.nav_home:
                tittle="Trang chu";
                fragmentName="home";
                fragment = new HomeFragment();
                Log.d(TAG, "displaySelectdScreen: Home");
                break;
            case R.id.nav_history:
                tittle="Lich su dat xe";
                fragmentName="history";
                fragment = new HistoryFragment();
                Log.d(TAG, "displaySelectdScreen: History");
                break;
                case R.id.nav_profile:
                    fragmentName="profile";
                    tittle="Ho so ";
                fragment = new ProfileFragment();
                break;
        }

//        replacing the fragment
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();
        }
        itemn.setChecked(true);
        getSupportActionBar().setTitle(tittle);
        drawer.closeDrawer(GravityCompat.START);
    }





    @Override
    public void goToProfile() {
        startActivity(new Intent(this, ProfileFragment.class));
    }

    @Override
    public void setAvatarAndName(String name, String u) {

    }
}