package com.example.bikerental.presenter;

import android.util.Log;

import com.example.bikerental.api.bookinghistory.HistoryApiHome;
import com.example.bikerental.api.model.history.BookingHistory;
import com.example.bikerental.contract.HistoryContract;
import com.example.bikerental.data.ApiResponseResult;
import com.example.bikerental.data.ApiResponseStatus;

public class HistoryPresenter implements HistoryContract.presenter {
    private HistoryContract.view view;
    private final String TAG = "HISTORY";

    public HistoryPresenter(HistoryContract.view view) {
        this.view = view;
    }

    @Override
    public void loadBookingHistory(String userId) {
        view.showDialog();
        new HistoryApiHome().getBookingHistory(userId, new ApiResponseResult() {
            @Override
            public void onResponse(ApiResponseStatus var1, Object var2, int var3) {
                view.hideDialog();
                switch (var1) {
                    case NETWORK_PROBLEM:
                        Log.d(TAG, "onResponse: getBookingHistory net");
                        break;
                    case FAIL:
                        Log.d(TAG, "onResponse: getBookingHistory fail");
                        break;
                    case SUCCESS:
                        BookingHistory bookingHistory=(BookingHistory)var2;
                        Log.d(TAG, "onResponse: getBookingHistory success and this is size= "+bookingHistory.getListBooking().size()+"\n"+"bike name= "+bookingHistory.getListBooking().get(0).getBike().getBikeName());
                        view.showHistoryRecycleView(bookingHistory);
                        break;
                }
            }
        });
    }
}
