package com.example.bikerental.storelist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.adapter.BilkeAdapter;
import com.example.bikerental.adapter.StoreAdapter;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.api.model.store.ListBranch;
import com.example.bikerental.api.model.store.Store;
import com.example.bikerental.contract.StoreActivityContract;
import com.example.bikerental.presenter.StoreActivityPresenter;
import com.example.bikerental.presenter.StoreListPresenter;

public class StoreListActivity extends AppCompatActivity implements StoreActivityContract.view {
RecyclerView store_list_recycleview;
StoreActivityPresenter presenter;
StoreAdapter storeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list);
        initLayout();
        //Lay danh sach cua hang
        presenter.getAllStore();
    }

    private void initLayout() {
        presenter=new StoreActivityPresenter(this);
        store_list_recycleview=findViewById(R.id.store_list_recycleview);
    }

    @Override
    public void setStoreItemToRecycleView(Store store) {
        StoreListPresenter list=new StoreListPresenter(store);
        storeAdapter = new StoreAdapter(list, new StoreAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ListBranch item) {
                Instance.branch=item;
                Intent intent=new Intent();
                intent.putExtra(Instance.tai_cua_hang,item.getAddress());
                intent.putExtra(Instance.branchID,item.getId());
                setResult(Activity.RESULT_OK,intent);
                finish();
            }

        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this
        );
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        store_list_recycleview.setLayoutManager(layoutManager);
        store_list_recycleview.setAdapter(storeAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
