package com.example.bikerental.bikedetail;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bikerental.ChooseTypeOfSignInActivity;
import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.example.bikerental.api.model.bike.ListBike;
import com.example.bikerental.contract.BikeDetailActivityContract;
import com.example.bikerental.getuserinfo.GetUserInfoActivity;
import com.example.bikerental.presenter.BikeDetailPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

public class BikeDetailActivity extends AppCompatActivity implements View.OnClickListener, BikeDetailActivityContract.DetailView {
    ImageView imageView;
    TextView bikeName, priceDetail, bikeType,storeAdress,deposit;
    RadioGroup radioGroup;
    RadioButton checkedRadio;
    Button next;
    FirebaseAuth auth;
    private final String TAG="KAKA";
    Intent intent;
  BikeDetailPresenter bikeDetailPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_detail2);
        intent = getIntent();
        init();
        getBikeDetail();
        Log.d(TAG, "Da vao detail: ");
    }

    public void init() {
        auth=FirebaseAuth.getInstance();
        imageView = findViewById(R.id.bike_image_detail);
        bikeName = findViewById(R.id.bike_name_detail);
        bikeType = findViewById(R.id.bike_type);
        next = findViewById(R.id.next);
        deposit=findViewById(R.id.deposit);
        storeAdress=findViewById(R.id.store_address);
        bikeDetailPresenter =new BikeDetailPresenter(auth,this);
        initOnClick();
    }

    private void initOnClick() {
        next.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.next:
                bikeDetailPresenter.checkUserSignIn();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void goToUserInfoInput() {
startActivity(new Intent(this, GetUserInfoActivity.class));
    }

    @Override
    public void goToSignInOption() {
        startActivity(new Intent(this, ChooseTypeOfSignInActivity.class));

    }

    @Override
    public void getBikeDetail() {
        ListBike bike=Instance.bike;
        if(bike!=null){
            Picasso.get().load(bike.getImages().get(0)).into(imageView);
            bikeName.setText(bike.getBikeName());
            if(bike.getTransmissionType()==0){
                bikeType.setText("Xe so");
            }else{
                bikeType.setText("Xe tay ga");
            }
            deposit.setText(bike.getMoneyDeposit().toString());
            storeAdress.setText(bike.getBranch().getAddress());
        }
    }
}
