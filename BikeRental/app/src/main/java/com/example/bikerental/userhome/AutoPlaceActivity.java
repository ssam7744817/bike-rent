package com.example.bikerental.userhome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.bikerental.Instance;
import com.example.bikerental.R;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;
import java.util.List;

public class AutoPlaceActivity extends AppCompatActivity {
    PlacesClient placesClient;
//    AutocompleteSupportFragment place_fragment;
    List<Place.Field> placeField = Arrays.asList(Place.Field.ID
            , Place.Field.NAME
            , Place.Field.ADDRESS);
    private final String TAG="SAM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_place);
//        setUpAutoPlaceComplete();
    }
    /**
     * Chức năng search gợi ý của google map
     */
    private void setUpAutoPlaceComplete() {
//        Places.initialize(this, Instance.API_KEY);
//        placesClient = Places.createClient(this);
//
//        place_fragment = (AutocompleteSupportFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.get_address_result);
//        place_fragment.setPlaceFields(placeField);
//        place_fragment.setHint(getResources().getString(R.string.dia_chi_nhan));
//        place_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(@NonNull Place place) {
//                place_fragment.setText(place.getAddress());
//
//            }
//
//            @Override
//            public void onError(@NonNull Status status) {
//                Toast.makeText(AutoPlaceActivity.this, "" + status.getStatusMessage(), Toast.LENGTH_SHORT).show();
//                Log.d(TAG, "onError: " + status.getStatusMessage());
//            }
//        });
    }
}
